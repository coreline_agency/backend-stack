'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createPayload = createPayload;
exports.sendToClient = sendToClient;
exports.sendToGuide = sendToGuide;
exports.sendBroadcast = sendBroadcast;

var _firebaseApp = require('../../firebase/firebase-app');

var _firebaseApp2 = _interopRequireDefault(_firebaseApp);

var _topic = require('./topic');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const OPTIONS = {
  priority: 'high'
};

function createPayload(body = '', action = '') {
  const payload = {
    notification: {
      body,
      clickAction: action,
      sound: 'default',
      badge: '1'
    }
  };

  return payload;
}

function sendToClient(clientId, payload) {
  const topic = (0, _topic.resolveClientTopic)(clientId);
  return _firebaseApp2.default.messaging().sendToTopic(topic, payload, OPTIONS);
}

function sendToGuide(guideId, payload) {
  const topic = (0, _topic.resolveGuideTopic)(guideId);
  return _firebaseApp2.default.messaging().sendToTopic(topic, payload, OPTIONS);
}

function sendBroadcast(payload) {
  const topic = (0, _topic.resolveBroadcastTopic)();
  return _firebaseApp2.default.messaging().sendToTopic(topic, payload, OPTIONS);
}