'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resolveClientTopic = resolveClientTopic;
exports.resolveGuideTopic = resolveGuideTopic;
exports.resolveBroadcastTopic = resolveBroadcastTopic;
function resolveClientTopic(clientId) {
  return `client.${clientId}`;
}

function resolveGuideTopic(guideId) {
  return `guide.${guideId}`;
}

function resolveBroadcastTopic() {
  return 'broadcast';
}