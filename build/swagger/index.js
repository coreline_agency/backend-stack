'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _swaggerJsdoc = require('swagger-jsdoc');

var _swaggerJsdoc2 = _interopRequireDefault(_swaggerJsdoc);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const swaggerSpec = (0, _swaggerJsdoc2.default)(_config2.default.options);
exports.default = swaggerSpec;