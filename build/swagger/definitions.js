/**
 * @swagger
 * parameters:
 *   query:
 *     name: query
 *     in: query
 *     description: query for text search
 *     type: string
 *   pagingOffset:
 *     name: page[offset]
 *     in: query
 *     description: paging offset
 *     type: integer
 *   pagingLimit:
 *     name: page[limit]
 *     in: query
 *     description: paging limit
 *     type: integer
 * definitions:
 *   JsonApiReference:
 *     type: object
 *     required:
 *     - type
 *     - id
 *     properties:
 *       type:
 *         type: string
 *       id:
 *         type: string
 *   JsonApiSingleRelationship:
 *     type: object
 *     required:
 *     - data
 *     properties:
 *       data:
 *         $ref: '#/definitions/JsonApiReference'
 *       links:
 *         type: object
 *       meta:
 *         type: object
 *   JsonApiCollectionRelationship:
 *     type: object
 *     required:
 *     - data
 *     properties:
 *       data:
 *         type: 'array'
 *         items:
 *           $ref: '#/definitions/JsonApiReference'
 *       links:
 *         type: object
 *       meta:
 *         type: object
 *   JsonApiPagingLinks:
 *     type: object
 *     properties:
 *       prev:
 *         type: string
 *       next:
 *         type: string
 */
"use strict";