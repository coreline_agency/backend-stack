'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lean = lean;
exports.leanPage = leanPage;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _sequelizeValues = require('sequelize-values');

var _sequelizeValues2 = _interopRequireDefault(_sequelizeValues);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const sequelize = new _sequelizeValues2.default();

function lean(target, name, descriptor) {
  const method = descriptor.value;

  descriptor.value = async function (...args) {
    const result = await method.apply(this, args);
    return sequelize.getValues(result);
  };

  return descriptor;
}

function leanPage(target, name, descriptor) {
  const method = descriptor.value;

  descriptor.value = async function (...args) {
    const result = await method.apply(this, args);
    return _lodash2.default.set(result, '_pageItems', sequelize.getValues(_lodash2.default.get(result, '_pageItems')));
  };

  return descriptor;
}