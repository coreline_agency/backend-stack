'use strict';

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _chai = require('chai');

var _encoder = require('../encoder');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Encoder', () => {
  const decodedDollarTestObj = {
    a: 1,
    a$b: 2,
    a$b$c: 3,
    a$b$c$d: 4
  };

  const encodedDollarTestObj = {
    a: 1,
    'a[dollar]b': 2,
    'a[dollar]b[dollar]c': 3,
    'a[dollar]b[dollar]c[dollar]d': 4
  };

  const decodedDotTestObj = {
    a: 1,
    'a.b': 2,
    'a.b.c': 3,
    'a.b.c.d': 4
  };

  const encodedDotTestObj = {
    a: 1,
    'a[dot]b': 2,
    'a[dot]b[dot]c': 3,
    'a[dot]b[dot]c[dot]d': 4
  };

  const decodedDotDollarTestObj = {
    a: 1,
    'a.$b': 2,
    'a.b$c': 3,
    'a.b$c.d': 4
  };

  const encodedDotDollarTestObj = {
    a: 1,
    'a[dot][dollar]b': 2,
    'a[dot]b[dollar]c': 3,
    'a[dot]b[dollar]c[dot]d': 4
  };

  describe('encode keys', () => {
    it('should replace \'$\' with \'[dollar]\'', () => {
      const actualEncodedObj = (0, _encoder.encodeKeys)(_lodash2.default.cloneDeep(decodedDollarTestObj));
      _chai.assert.deepEqual(actualEncodedObj, encodedDollarTestObj);
    });

    it('should replace \'.\' with \'[dot]\'', () => {
      const actualEncodedObj = (0, _encoder.encodeKeys)(_lodash2.default.cloneDeep(decodedDotTestObj));
      _chai.assert.deepEqual(actualEncodedObj, encodedDotTestObj);
    });

    it('should replace \'$\' with \'[dollar]\' and ' + '\'.\' with \'[dot]\'', () => {
      const actualEncodedObj = (0, _encoder.encodeKeys)(_lodash2.default.cloneDeep(decodedDotDollarTestObj));
      _chai.assert.deepEqual(actualEncodedObj, encodedDotDollarTestObj);
    });
  });

  describe('decode keys', () => {
    it('should replace \'[dollar]\' with \'$\'', () => {
      const actualDecodedObj = (0, _encoder.decodeKeys)(_lodash2.default.cloneDeep(encodedDollarTestObj));
      _chai.assert.deepEqual(actualDecodedObj, decodedDollarTestObj);
    });

    it('should replace \'[dot]\' with \'.\'', () => {
      const actualDecodedObj = (0, _encoder.decodeKeys)(_lodash2.default.cloneDeep(encodedDotTestObj));
      _chai.assert.deepEqual(actualDecodedObj, decodedDotTestObj);
    });

    it('should replace \'[dollar]\' with \'$\' and ' + '\'[dot]\' with \'.\'', () => {
      const actualDecodedObj = (0, _encoder.decodeKeys)(_lodash2.default.cloneDeep(encodedDotDollarTestObj));
      _chai.assert.deepEqual(actualDecodedObj, decodedDotDollarTestObj);
    });
  });
});