'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PgTypes = undefined;

var _pg = require('pg');

var _pg2 = _interopRequireDefault(_pg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const timestampOID = 1114;

let PgTypes = exports.PgTypes = class PgTypes {
  initialize() {
    const types = _pg2.default.types;

    types.setTypeParser(timestampOID, stringValue => stringValue.replace(' ', 'T'));
  }
};
exports.default = new PgTypes();