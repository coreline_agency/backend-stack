'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  operatorsAliases: {
    $eq: _sequelize2.default.Op.eq,
    $ne: _sequelize2.default.Op.ne,
    $gte: _sequelize2.default.Op.gte,
    $gt: _sequelize2.default.Op.gt,
    $lte: _sequelize2.default.Op.lte,
    $lt: _sequelize2.default.Op.lt,
    $not: _sequelize2.default.Op.not,
    $in: _sequelize2.default.Op.in,
    $notIn: _sequelize2.default.Op.notIn,
    $is: _sequelize2.default.Op.is,
    $like: _sequelize2.default.Op.like,
    $notLike: _sequelize2.default.Op.notLike,
    $iLike: _sequelize2.default.Op.iLike,
    $notILike: _sequelize2.default.Op.notILike,
    $regexp: _sequelize2.default.Op.regexp,
    $notRegexp: _sequelize2.default.Op.notRegexp,
    $iRegexp: _sequelize2.default.Op.iRegexp,
    $notIRegexp: _sequelize2.default.Op.notIRegexp,
    $between: _sequelize2.default.Op.between,
    $notBetween: _sequelize2.default.Op.notBetween,
    $overlap: _sequelize2.default.Op.overlap,
    $contains: _sequelize2.default.Op.contains,
    $contained: _sequelize2.default.Op.contained,
    $adjacent: _sequelize2.default.Op.adjacent,
    $strictLeft: _sequelize2.default.Op.strictLeft,
    $strictRight: _sequelize2.default.Op.strictRight,
    $noExtendRight: _sequelize2.default.Op.noExtendRight,
    $noExtendLeft: _sequelize2.default.Op.noExtendLeft,
    $and: _sequelize2.default.Op.and,
    $or: _sequelize2.default.Op.or,
    $any: _sequelize2.default.Op.any,
    $all: _sequelize2.default.Op.all,
    $values: _sequelize2.default.Op.values,
    $col: _sequelize2.default.Op.col
  }
};