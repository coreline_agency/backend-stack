'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Database = undefined;

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _db = require('./config/db');

var _db2 = _interopRequireDefault(_db);

var _env = require('../env');

var _logging = require('../logging');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let Database = exports.Database = class Database {
  constructor() {
    this.sequelize = new _sequelize2.default({
      username: (0, _env.requireEnvString)('DB_USERNAME'),
      password: (0, _env.requireEnvString)('DB_PASSWORD'),
      database: (0, _env.requireEnvString)('DB_NAME'),
      dialect: 'postgres',
      protocol: 'postgres',
      host: (0, _env.requireEnvString)('DB_HOST'),
      port: (0, _env.requireEnvString)('DB_PORT', '5432'),
      operatorsAliases: _db2.default.operatorsAliases
    });
  }

  connect() {
    return this.sequelize.authenticate().then(() => {
      _logging.logger.info('Database connection has been established successfully.');
    }).catch(err => {
      _logging.logger.error(err, 'Unable to connect to the database');
    });
  }

  getInstance() {
    return this.sequelize;
  }
};
exports.default = new Database();