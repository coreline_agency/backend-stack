'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encodeKeys = encodeKeys;
exports.decodeKeys = decodeKeys;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const escapeCharacter = '[';
const escapedEscapeCharacter = escapeCharacter + escapeCharacter;
const escapeCharacterRegExp = /\[/g;

const encodingMap = {
  $: '[dollar]',
  '.': '[dot]'
};

const decodingMap = _lodash2.default.invert(encodingMap);

const encodingRegExpMap = {
  $: /\$/g,
  '.': /\./g
};

const decodingRegExpMap = {
  '[dollar]': /\[dollar]/g,
  '[dot]': /\[dot]/g
};

/**
 * Encodes object keys.
 * @param obj Object to encode
 * @returns New encoded object
 */
function encodeKeys(obj) {
  const encodeCharacters = _lodash2.default.keys(encodingMap);

  return _lodash2.default.mapKeys(obj, (value, key) => {
    let newKey = key.replace(escapeCharacterRegExp, escapedEscapeCharacter);
    for (const encodeChar of encodeCharacters) {
      newKey = newKey.replace(encodingRegExpMap[encodeChar], encodingMap[encodeChar]);
    }
    return newKey;
  });
}

/**
 * Decodes object keys.
 * @param obj Object to decode
 * @returns New decoded object
 */
function decodeKeys(obj) {
  const decodeStrings = _lodash2.default.keys(decodingMap);

  return _lodash2.default.mapKeys(obj, (value, key) => {
    let newKey = key;
    for (const decodeStr of decodeStrings) {
      newKey = newKey.replace(decodingRegExpMap[decodeStr], decodingMap[decodeStr]);
    }
    newKey = newKey.replace(escapeCharacterRegExp, escapedEscapeCharacter);
    return newKey;
  });
}