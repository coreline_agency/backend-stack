'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (ioFilter) {
  const sequelizeFilter = {};
  _lodash2.default.each(ioFilter, (filterValue, filter) => {
    if (_lodash2.default.isObject(filterValue)) {
      _lodash2.default.each(filterValue, (value, operator) => {
        const sequelizeOperator = ioSequelizeFilterMapping[operator];
        if (sequelizeOperator) {
          sequelizeFilter[filter] = { [`${sequelizeOperator}`]: value };
        }
      });
    } else {
      sequelizeFilter[filter] = filterValue;
    }
  });
  return sequelizeFilter;
};

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ioSequelizeFilterMapping = {
  gt: '$gt',
  lt: '$lt'
};