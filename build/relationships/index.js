'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mapRelationships = mapRelationships;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function mapRelationships(data, relationships) {
  const newData = _lodash2.default.cloneDeep(data);

  _lodash2.default.forEach(relationships, relationship => {
    if (!_lodash2.default.isUndefined(_lodash2.default.get(newData, relationship))) {
      _lodash2.default.set(newData, `${relationship}Id`, _lodash2.default.get(newData, relationship));
      _lodash2.default.unset(newData, relationship);
    }
  });

  return newData;
}