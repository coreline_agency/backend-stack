'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setLocals = exports.getLocals = undefined;

var _service = require('./service');

exports.getLocals = _service.getLocals;
exports.setLocals = _service.setLocals;