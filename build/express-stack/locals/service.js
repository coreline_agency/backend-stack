'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLocals = getLocals;
exports.setLocals = setLocals;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Read data stored inside particular `namespace` of `obj.locals`.
 */
function getLocals(obj, namespace, defaultValue = undefined) {
  return _lodash2.default.get(obj, `locals.${namespace}`, defaultValue);
}

/*
 * Store `data` inside `obj.locals[namespace]`.
 *
 * `obj` can be any object, but is usually either request or response.
 * `namespace` groups all data belonging to one middleware - for example,
 * authentication middleware might save user id to `req.locals.auth.userId`.
 */
/*
  Functions in this module simplify interaction between middleware functions
  and request/response.locals. Sole purpose of `locals` is storing and
  sharing intermediate data between middleware.
*/
function setLocals(obj, namespace, data) {
  if (!obj.locals) obj.locals = {};
  if (data === undefined) return _lodash2.default.unset(obj, `locals.${namespace}`);
  if (obj.locals[namespace]) return _lodash2.default.merge(obj.locals[namespace], data);
  return _lodash2.default.set(obj, `locals.${namespace}`, data);
}