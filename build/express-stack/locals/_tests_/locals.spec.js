'use strict';

var _chai = require('chai');

var _service = require('../service');

describe('Modify request/response locals', () => {
  describe('setLocals', () => {
    it('adds arbitrary data to object even if it has no property `locals`', () => {
      const obj = {};
      (0, _service.setLocals)(obj, 'test', { x: 10, y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('adds arbitrary data to `obj.locals[namespace]`', () => {
      const obj = { locals: {} };
      (0, _service.setLocals)(obj, 'test', { x: 10, y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('extends data already present in `obj.locals[namespace]`', () => {
      const obj = { locals: { test: { x: 10 } } };
      (0, _service.setLocals)(obj, 'test', { y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('merges new data with present data', () => {
      const obj = { locals: { test: { foo: { bar: 100 } } } };
      (0, _service.setLocals)(obj, 'test', { foo: { baz: 200 } });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.foo.bar, 100);
      _chai.assert.strictEqual(obj.locals.test.foo.baz, 200);
    });

    it('does not affect other namespaces', () => {
      const obj = { locals: { other: { x: 10 } } };
      (0, _service.setLocals)(obj, 'test', { y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.other && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.other.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('setting property to undefined value unsets the property', () => {
      const obj = { locals: { test: { x: 10 } } };
      (0, _service.setLocals)(obj, 'test');
      _chai.assert.isUndefined(obj.locals.test);
    });
  });

  describe('getLocals', () => {
    it('returns `undefined` if any path is missing', () => {
      const obj1 = {};
      const obj2 = { locals: null };
      _chai.assert.isUndefined((0, _service.getLocals)(obj1, 'test'));
      _chai.assert.isUndefined((0, _service.getLocals)(obj2, 'test'));
    });

    it('returns data from `obj.locals[namespace]`', () => {
      const obj = { locals: { test: { x: 10, y: 20 } } };
      const result = (0, _service.getLocals)(obj, 'test');
      _chai.assert.isOk(result);
      _chai.assert.strictEqual(result.x, 10);
      _chai.assert.strictEqual(result.y, 20);
    });
  });
});