'use strict';

var _chai = require('chai');

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _index = require('../index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

_sinon2.default.assert.expose(_chai.assert, { prefix: '' });

describe('Cache communication', () => {
  function getSavedDevelopers() {
    return [{
      _id: '123456789',
      name: 'lovro',
      userId: '1'
    }, {
      _id: '987654321',
      name: 'shoutem',
      userId: '2'
    }, {
      _id: '1241244',
      name: 'A',
      userId: '3'
    }];
  }

  function getByUserId(userId, cb) {
    const savedDevelopers = getSavedDevelopers();
    for (let i = 0; i < savedDevelopers.length; i++) {
      if (savedDevelopers[i].userId === userId) {
        cb(null, savedDevelopers[i]);
      }
    }
  }

  function getByMultipleArgs(...args) {
    const cb = args[args.length - 1];
    const savedDevelopers = getSavedDevelopers();
    for (let i = 0; i < savedDevelopers.length; i++) {
      if (savedDevelopers[i].userId === args[0]) {
        cb(null, savedDevelopers[i]);
      }
    }
  }

  function updateUser(userId, newName, cb) {
    const savedDevelopers = getSavedDevelopers();
    for (let i = 0; i < savedDevelopers.length; i++) {
      if (savedDevelopers[i].userId === userId) {
        savedDevelopers[i].name = newName;
        cb(null, savedDevelopers[i]);
      }
    }
    return null;
  }

  function getByUserIdPromise(userId) {
    const savedDevelopers = getSavedDevelopers();
    for (let i = 0; i < savedDevelopers.length; i++) {
      if (savedDevelopers[i].userId === userId) {
        return _bluebird2.default.resolve(savedDevelopers[i]);
      }
    }
    return _bluebird2.default.reject();
  }

  function updateUserPromise(userId, newName) {
    const savedDevelopers = getSavedDevelopers();
    for (let i = 0; i < savedDevelopers.length; i++) {
      if (savedDevelopers[i].userId === userId) {
        savedDevelopers[i].name = newName;
        return _bluebird2.default.resolve(savedDevelopers[i]);
      }
    }
    return _bluebird2.default.reject();
  }

  function getByMultipleArgsPromise(...args) {
    const savedDevelopers = getSavedDevelopers();
    for (let i = 0; i < savedDevelopers.length; i++) {
      if (savedDevelopers[i].userId === args[0]) {
        return _bluebird2.default.resolve(savedDevelopers[i]);
      }
    }
    return _bluebird2.default.reject();
  }

  function getDeveloper() {
    return {
      _id: '123456789',
      name: 'lovro',
      userId: '1'
    };
  }

  function getUpdatedDeveloper() {
    return {
      _id: '123456789',
      name: 'shoutem',
      userId: '1'
    };
  }

  const constants = {
    USER_ID: '1',
    USER_ID_SHOUTEM: '2',
    DEVELOPER_INVALIDATION_KEY: 'developers',
    APPLICATION_INVALIDATION_KEY: 'applications',
    NEW_NAME: 'shoutem'
  };

  describe('Using decorators with callback', () => {
    describe('get user', () => {
      it('returns user with the given userId from cache (when user exists in cache)', async () => {
        var _dec, _desc, _value, _class;

        const expectedDeveloper = getDeveloper();

        const mockCache = {
          getAsync: _sinon2.default.stub().withArgs(`userId:${constants.USER_ID}`).resolves(expectedDeveloper)
        };

        let MockRepository = (_dec = (0, _index.cacheDecoratorLegacy)(args => `userId:${args}`, id => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id}`, `${constants.APPLICATION_INVALIDATION_KEY}:${id}`], mockCache), (_class = class MockRepository {
          get(userId, cb) {
            return getByUserId(userId, cb);
          }
        }, (_applyDecoratedDescriptor(_class.prototype, 'get', [_dec], Object.getOwnPropertyDescriptor(_class.prototype, 'get'), _class.prototype)), _class));


        await new _bluebird2.default((resolve, reject) => {
          const cb = (err, result) => {
            try {
              _chai.assert.isTrue(_lodash2.default.isNil(err));
              _chai.assert.calledWithExactly(mockCache.getAsync, `userId:${constants.USER_ID}`);
              _chai.assert.deepEqual(expectedDeveloper, result);

              resolve();
            } catch (err) {
              reject(err);
            }
          };
          const repo = new MockRepository();
          repo.get(constants.USER_ID, cb);
        });
      });

      it('returns user with the given userId from cache (when there is no specific user in cache)', async () => {
        var _dec2, _desc2, _value2, _class2;

        const expectedDeveloper = getDeveloper();

        const mockCache = {
          getAsync: _sinon2.default.stub().withArgs(`userId:${constants.USER_ID}`).resolves(null),
          setAsync: _sinon2.default.stub().withArgs(`userId:${constants.USER_ID}`, {
            _id: '123456789',
            name: 'lovro',
            userId: '1'
          })
        };

        let MockRepository = (_dec2 = (0, _index.cacheDecoratorLegacy)(args => `userId:${args}`, id => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id}`, `${constants.APPLICATION_INVALIDATION_KEY}:${id}`], mockCache), (_class2 = class MockRepository {
          get(userId, cb) {
            return getByUserId(userId, cb);
          }
        }, (_applyDecoratedDescriptor(_class2.prototype, 'get', [_dec2], Object.getOwnPropertyDescriptor(_class2.prototype, 'get'), _class2.prototype)), _class2));


        await new _bluebird2.default((resolve, reject) => {
          const cb = (err, result) => {
            try {
              _chai.assert.isTrue(_lodash2.default.isNil(err));
              _chai.assert.calledWithExactly(mockCache.getAsync, `userId:${constants.USER_ID}`);
              _chai.assert.deepEqual(expectedDeveloper, result);

              resolve();
            } catch (err) {
              reject(err);
            }
          };
          const repo = new MockRepository();
          repo.get(constants.USER_ID, cb);
        });
      });

      it('set valid invalidation keys when there isn\'t any dependency key for given invalidation key', async () => {
        var _dec3, _desc3, _value3, _class3;

        const expectedDeveloper = getDeveloper();
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          setAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`).resolves(null);
        mockCache.getAsync.withArgs(`userId:${constants.USER_ID}`).resolves(null);
        mockCache.setAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]).resolves(null);
        mockCache.setAsync.withArgs(`userId:${constants.USER_ID}`, {
          _id: '123456789',
          name: 'lovro',
          userId: '1'
        });
        let MockRepository = (_dec3 = (0, _index.cacheDecoratorLegacy)(args => `userId:${args}`, id => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id}`, `${constants.APPLICATION_INVALIDATION_KEY}:${id}`], mockCache), (_class3 = class MockRepository {
          get(userId, cb) {
            return getByUserId(userId, cb);
          }
        }, (_applyDecoratedDescriptor(_class3.prototype, 'get', [_dec3], Object.getOwnPropertyDescriptor(_class3.prototype, 'get'), _class3.prototype)), _class3));


        await new _bluebird2.default((resolve, reject) => {
          const cb = (err, result) => {
            try {
              _chai.assert.calledWithExactly(mockCache.setAsync, `userId:${constants.USER_ID}`, {
                _id: '123456789',
                name: 'lovro',
                userId: '1'
              });
              _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]);
              _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.APPLICATION_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]);
              _chai.assert.isTrue(_lodash2.default.isNil(err));
              _chai.assert.deepEqual(expectedDeveloper, result);

              resolve();
            } catch (err) {
              reject(err);
            }
          };
          const repo = new MockRepository();
          repo.get(constants.USER_ID, cb);
        });
      });

      it('set valid invalidation keys when dependency key for given invalidation key exists', async () => {
        var _dec4, _desc4, _value4, _class4;

        const shoutemDeveloper = {
          _id: '987654321',
          name: 'shoutem',
          userId: '2'
        };
        const existingDependencyKeys = [`userId:${constants.USER_ID}`];
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          setAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(`userId:${constants.USER_ID_SHOUTEM}`).resolves(null);
        mockCache.getAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY).resolves(existingDependencyKeys);
        mockCache.setAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY, [`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]).resolves(null);
        mockCache.setAsync.withArgs((`userId:${constants.USER_ID_SHOUTEM}`, {
          _id: '987654321',
          name: 'shoutem',
          userId: '2'
        })).resolves(null);

        let MockRepository = (_dec4 = (0, _index.cacheDecoratorLegacy)(args => `userId:${args}`, () => [constants.DEVELOPER_INVALIDATION_KEY], mockCache), (_class4 = class MockRepository {
          get(userId, cb) {
            return getByUserId(userId, cb);
          }
        }, (_applyDecoratedDescriptor(_class4.prototype, 'get', [_dec4], Object.getOwnPropertyDescriptor(_class4.prototype, 'get'), _class4.prototype)), _class4));


        await new _bluebird2.default((resolve, reject) => {
          const cb = (err, result) => {
            try {
              _chai.assert.calledWithExactly(mockCache.setAsync, `userId:${constants.USER_ID_SHOUTEM}`, shoutemDeveloper);
              _chai.assert.calledWithExactly(mockCache.setAsync, constants.DEVELOPER_INVALIDATION_KEY, [`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]);
              _chai.assert.isTrue(_lodash2.default.isNil(err));
              _chai.assert.deepEqual(shoutemDeveloper, result);

              resolve();
            } catch (err) {
              reject(err);
            }
          };
          const repo = new MockRepository();
          repo.get(constants.USER_ID_SHOUTEM, cb);
        });
      });

      it('set valid invalidation key with multiple parameters', async () => {
        var _dec5, _desc5, _value5, _class5;

        const expectedDeveloper = getDeveloper();
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          setAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`).resolves(null);
        mockCache.getAsync.withArgs(`userId:${constants.USER_ID}`).resolves(null);
        mockCache.setAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID_SHOUTEM}`, [`userId:${constants.USER_ID}`]).resolves(null);
        mockCache.setAsync.withArgs(`userId:${constants.USER_ID}`, {
          _id: '123456789',
          name: 'lovro',
          userId: '1'
        });

        let MockRepository = (_dec5 = (0, _index.cacheDecoratorLegacy)(args => `userId:${args}`, (id1, id2) => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id1}`, `${constants.DEVELOPER_INVALIDATION_KEY}:${id2}`], mockCache), (_class5 = class MockRepository {
          get(userId, userIdShoutem, cb) {
            return getByMultipleArgs(userId, userIdShoutem, cb);
          }
        }, (_applyDecoratedDescriptor(_class5.prototype, 'get', [_dec5], Object.getOwnPropertyDescriptor(_class5.prototype, 'get'), _class5.prototype)), _class5));


        await new _bluebird2.default((resolve, reject) => {
          const cb = (err, result) => {
            try {
              _chai.assert.calledWithExactly(mockCache.setAsync, `userId:${constants.USER_ID}`, {
                _id: '123456789',
                name: 'lovro',
                userId: '1'
              });
              _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]);
              _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID_SHOUTEM}`, [`userId:${constants.USER_ID}`]);
              _chai.assert.isTrue(_lodash2.default.isNil(err));
              _chai.assert.deepEqual(expectedDeveloper, result);

              resolve();
            } catch (err) {
              reject(err);
            }
          };

          const repo = new MockRepository();
          repo.get(constants.USER_ID, constants.USER_ID_SHOUTEM, cb);
        });
      });
    });

    describe('update user', () => {
      it('delete specific invalidation keys', async () => {
        var _dec6, _desc6, _value6, _class6;

        const expectedUpdatedDeveloper = getUpdatedDeveloper();

        const mockCache = {
          getAsync: _sinon2.default.stub(),
          delAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY).resolves([`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]);
        mockCache.getAsync.withArgs(constants.APPLICATION_INVALIDATION_KEY).resolves([`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]);
        mockCache.delAsync.withArgs(`userId:${constants.USER_ID}`);
        mockCache.delAsync.withArgs(`userId:${constants.USER_ID_SHOUTEM}`);
        mockCache.delAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY);
        mockCache.delAsync.withArgs(constants.APPLICATION_INVALIDATION_KEY);

        let MockRepository = (_dec6 = (0, _index.invalidateCacheDecoratorLegacy)(() => [constants.DEVELOPER_INVALIDATION_KEY, constants.APPLICATION_INVALIDATION_KEY], mockCache), (_class6 = class MockRepository {
          update(userId, newName, cb) {
            return updateUser(userId, newName, cb);
          }
        }, (_applyDecoratedDescriptor(_class6.prototype, 'update', [_dec6], Object.getOwnPropertyDescriptor(_class6.prototype, 'update'), _class6.prototype)), _class6));


        await new _bluebird2.default((resolve, reject) => {
          const cb = (err, result) => {
            try {
              _chai.assert.isTrue(_lodash2.default.isNil(err));
              _chai.assert.deepEqual(expectedUpdatedDeveloper, result);
              _chai.assert.calledWithExactly(mockCache.getAsync, constants.DEVELOPER_INVALIDATION_KEY);
              _chai.assert.calledWithExactly(mockCache.getAsync, constants.APPLICATION_INVALIDATION_KEY);
              _chai.assert.calledWithExactly(mockCache.delAsync, `userId:${constants.USER_ID}`);
              _chai.assert.calledWithExactly(mockCache.delAsync, `userId:${constants.USER_ID_SHOUTEM}`);
              _chai.assert.calledWithExactly(mockCache.delAsync, constants.DEVELOPER_INVALIDATION_KEY);
              _chai.assert.calledWithExactly(mockCache.delAsync, constants.APPLICATION_INVALIDATION_KEY);

              resolve();
            } catch (err) {
              reject(err);
            }
          };
          const repo = new MockRepository();
          repo.update(constants.USER_ID, constants.NEW_NAME, cb);
        });
      });
    });
  });

  describe('Using decorators with promise', () => {
    describe('get user', () => {
      it('returns user with the given userId from cache (when user exists in cache)', async () => {
        var _dec7, _desc7, _value7, _class7;

        const expectedDeveloper = getDeveloper();

        const mockCache = {
          getAsync: _sinon2.default.stub().withArgs(`userId:${constants.USER_ID}`).resolves(expectedDeveloper)
        };

        let MockRepository = (_dec7 = (0, _index.cacheDecorator)(args => `userId:${args}`, id => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id}`, `${constants.APPLICATION_INVALIDATION_KEY}:${id}`], mockCache), (_class7 = class MockRepository {
          async get(userId) {
            return getByUserIdPromise(userId);
          }
        }, (_applyDecoratedDescriptor(_class7.prototype, 'get', [_dec7], Object.getOwnPropertyDescriptor(_class7.prototype, 'get'), _class7.prototype)), _class7));


        const repo = new MockRepository();
        const cachedDeveloper = await repo.get(constants.USER_ID);
        _chai.assert.calledWithExactly(mockCache.getAsync, `userId:${constants.USER_ID}`);
        _chai.assert.deepEqual(expectedDeveloper, cachedDeveloper);
      });

      it('returns user with the given userId from cache (when there is no specific user in cache)', async () => {
        var _dec8, _desc8, _value8, _class8;

        const expectedDeveloper = getDeveloper();

        const mockCache = {
          getAsync: _sinon2.default.stub().withArgs(`userId:${constants.USER_ID}`).resolves(null),
          setAsync: _sinon2.default.stub().withArgs(`userId:${constants.USER_ID}`, {
            _id: '123456789',
            name: 'lovro',
            userId: '1'
          })
        };

        let MockRepository = (_dec8 = (0, _index.cacheDecorator)(args => `userId:${args}`, id => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id}`, `${constants.APPLICATION_INVALIDATION_KEY}:${id}`], mockCache), (_class8 = class MockRepository {
          async get(userId) {
            return getByUserIdPromise(userId);
          }
        }, (_applyDecoratedDescriptor(_class8.prototype, 'get', [_dec8], Object.getOwnPropertyDescriptor(_class8.prototype, 'get'), _class8.prototype)), _class8));


        const repo = new MockRepository();
        const cachedDeveloper = await repo.get(constants.USER_ID);
        _chai.assert.calledWithExactly(mockCache.getAsync, `userId:${constants.USER_ID}`);
        _chai.assert.deepEqual(expectedDeveloper, cachedDeveloper);
      });

      it('set valid invalidation keys when there isn\'t any dependency key for given invalidation key', async () => {
        var _dec9, _desc9, _value9, _class9;

        const expectedDeveloper = getDeveloper();
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          setAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`).resolves(null);
        mockCache.getAsync.withArgs(`userId:${constants.USER_ID}`).resolves(null);
        mockCache.setAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]).resolves(null);
        mockCache.setAsync.withArgs(`userId:${constants.USER_ID}`, {
          _id: '123456789',
          name: 'lovro',
          userId: '1'
        });

        let MockRepository = (_dec9 = (0, _index.cacheDecorator)(args => `userId:${args}`, id => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id}`, `${constants.APPLICATION_INVALIDATION_KEY}:${id}`], mockCache), (_class9 = class MockRepository {
          get(userId) {
            return getByUserIdPromise(userId);
          }
        }, (_applyDecoratedDescriptor(_class9.prototype, 'get', [_dec9], Object.getOwnPropertyDescriptor(_class9.prototype, 'get'), _class9.prototype)), _class9));


        const repo = new MockRepository();
        const cachedDeveloper = await repo.get(constants.USER_ID);
        _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]);
        _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.APPLICATION_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]);
        _chai.assert.deepEqual(expectedDeveloper, cachedDeveloper);
      });

      it('set valid invalidation keys when dependency key for given invalidation key exists', async () => {
        var _dec10, _desc10, _value10, _class10;

        const shoutemDeveloper = {
          _id: '987654321',
          name: 'shoutem',
          userId: '2'
        };
        const existingDependencyKeys = [`userId:${constants.USER_ID}`];
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          setAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(`userId:${constants.USER_ID_SHOUTEM}`).resolves(null);
        mockCache.getAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY).resolves(existingDependencyKeys);
        mockCache.setAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY, [`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]).resolves(null);
        mockCache.setAsync.withArgs((`userId:${constants.USER_ID_SHOUTEM}`, {
          _id: '987654321',
          name: 'shoutem',
          userId: '2'
        })).resolves(null);

        let MockRepository = (_dec10 = (0, _index.cacheDecorator)(args => `userId:${args}`, () => [constants.DEVELOPER_INVALIDATION_KEY], mockCache), (_class10 = class MockRepository {
          get(userId) {
            return getByUserIdPromise(userId);
          }
        }, (_applyDecoratedDescriptor(_class10.prototype, 'get', [_dec10], Object.getOwnPropertyDescriptor(_class10.prototype, 'get'), _class10.prototype)), _class10));


        const repo = new MockRepository();
        const cachedDeveloper = await repo.get(constants.USER_ID_SHOUTEM);
        _chai.assert.calledWithExactly(mockCache.setAsync, `userId:${constants.USER_ID_SHOUTEM}`, shoutemDeveloper);
        _chai.assert.calledWithExactly(mockCache.setAsync, constants.DEVELOPER_INVALIDATION_KEY, [`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]);
        _chai.assert.deepEqual(shoutemDeveloper, cachedDeveloper);
      });

      it('set valid invalidation key with multiple parameters', async () => {
        var _dec11, _desc11, _value11, _class11;

        const expectedDeveloper = getDeveloper();
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          setAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`).resolves(null);
        mockCache.getAsync.withArgs(`userId:${constants.USER_ID}`).resolves(null);
        mockCache.setAsync.withArgs(`${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID_SHOUTEM}`, [`userId:${constants.USER_ID}`]).resolves(null);
        mockCache.setAsync.withArgs(`userId:${constants.USER_ID}`, {
          _id: '123456789',
          name: 'lovro',
          userId: '1'
        });

        let MockRepository = (_dec11 = (0, _index.cacheDecorator)(args => `userId:${args}`, (id1, id2) => [`${constants.DEVELOPER_INVALIDATION_KEY}:${id1}`, `${constants.DEVELOPER_INVALIDATION_KEY}:${id2}`], mockCache), (_class11 = class MockRepository {
          get(userId, userIdShoutem) {
            return getByMultipleArgsPromise(userId, userIdShoutem);
          }
        }, (_applyDecoratedDescriptor(_class11.prototype, 'get', [_dec11], Object.getOwnPropertyDescriptor(_class11.prototype, 'get'), _class11.prototype)), _class11));


        const repo = new MockRepository();
        const result = await repo.get(constants.USER_ID, constants.USER_ID_SHOUTEM);
        _chai.assert.calledWithExactly(mockCache.setAsync, `userId:${constants.USER_ID}`, {
          _id: '123456789',
          name: 'lovro',
          userId: '1'
        });
        _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID}`, [`userId:${constants.USER_ID}`]);
        _chai.assert.calledWithExactly(mockCache.setAsync, `${constants.DEVELOPER_INVALIDATION_KEY}:${constants.USER_ID_SHOUTEM}`, [`userId:${constants.USER_ID}`]);
        _chai.assert.deepEqual(expectedDeveloper, result);
      });
    });

    describe('update user', () => {
      it('delete specific invalidation keys', async () => {
        var _dec12, _desc12, _value12, _class12;

        const expectedUpdatedDeveloper = getUpdatedDeveloper();
        const mockCache = {
          getAsync: _sinon2.default.stub(),
          delAsync: _sinon2.default.stub()
        };

        mockCache.getAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY).resolves([`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]);
        mockCache.getAsync.withArgs(constants.APPLICATION_INVALIDATION_KEY).resolves([`userId:${constants.USER_ID}`, `userId:${constants.USER_ID_SHOUTEM}`]);
        mockCache.delAsync.withArgs(`userId:${constants.USER_ID}`);
        mockCache.delAsync.withArgs(`userId:${constants.USER_ID_SHOUTEM}`);
        mockCache.delAsync.withArgs(constants.DEVELOPER_INVALIDATION_KEY);
        mockCache.delAsync.withArgs(constants.APPLICATION_INVALIDATION_KEY);

        let MockRepository = (_dec12 = (0, _index.invalidateCacheDecorator)(() => [constants.DEVELOPER_INVALIDATION_KEY, constants.APPLICATION_INVALIDATION_KEY], mockCache), (_class12 = class MockRepository {
          update(userId, newName) {
            return updateUserPromise(userId, newName);
          }
        }, (_applyDecoratedDescriptor(_class12.prototype, 'update', [_dec12], Object.getOwnPropertyDescriptor(_class12.prototype, 'update'), _class12.prototype)), _class12));


        const repo = new MockRepository();
        const updatedDeveloper = await repo.update(constants.USER_ID, constants.NEW_NAME);

        _chai.assert.deepEqual(expectedUpdatedDeveloper, updatedDeveloper);
        _chai.assert.calledWithExactly(mockCache.getAsync, constants.DEVELOPER_INVALIDATION_KEY);
        _chai.assert.calledWithExactly(mockCache.getAsync, constants.APPLICATION_INVALIDATION_KEY);
        _chai.assert.calledWithExactly(mockCache.delAsync, `userId:${constants.USER_ID}`);
        _chai.assert.calledWithExactly(mockCache.delAsync, `userId:${constants.USER_ID_SHOUTEM}`);
        _chai.assert.calledWithExactly(mockCache.delAsync, constants.DEVELOPER_INVALIDATION_KEY);
        _chai.assert.calledWithExactly(mockCache.delAsync, constants.APPLICATION_INVALIDATION_KEY);
      });
    });
  });
});