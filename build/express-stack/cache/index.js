'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invalidateCacheDecoratorLegacy = exports.invalidateCacheDecorator = exports.cacheDecoratorLegacy = exports.cacheDecorator = undefined;

var _cacheDecorators = require('./cache-decorators');

exports.cacheDecorator = _cacheDecorators.cacheDecorator;
exports.cacheDecoratorLegacy = _cacheDecorators.cacheDecoratorLegacy;
exports.invalidateCacheDecorator = _cacheDecorators.invalidateCacheDecorator;
exports.invalidateCacheDecoratorLegacy = _cacheDecorators.invalidateCacheDecoratorLegacy;