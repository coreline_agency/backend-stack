'use strict';

var _chai = require('chai');

var _pagedCollection = require('../paged-collection');

var _pagedCollection2 = _interopRequireDefault(_pagedCollection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('PagedCollection', () => {
  describe('iterator', () => {
    it('iterates over collectionItems', () => {
      const collection = [{
        id: 1
      }, {
        id: 2
      }, {
        id: 3
      }];
      const pagedCollection = new _pagedCollection2.default(collection, {});
      let counter = 1;
      for (const item of pagedCollection) {
        _chai.assert.strictEqual(item.id, counter);
        counter++;
      }
    });
  });
});