'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let PagedCollection = class PagedCollection {
  constructor(pageItems, pageInfo) {
    this._pageItems = pageItems;
    this._pageInfo = pageInfo;
  }

  static createFromNPlus1Result(resultItems, requestedPage) {
    const hasNext = resultItems.length > requestedPage.limit;
    const items = _lodash2.default.take(resultItems, requestedPage.limit);
    return new PagedCollection(items, _extends({}, requestedPage, { hasNext }));
  }

  [Symbol.iterator]() {
    let index = -1;
    const pageItems = this._pageItems;

    return {
      next: () => ({ value: pageItems[++index], done: !(index in pageItems) })
    };
  }

  getPageInfo() {
    return this._pageInfo;
  }

  getPageItems() {
    return this._pageItems;
  }

  setPageItems(items) {
    this._pageItems = items;
  }
};
exports.default = PagedCollection;