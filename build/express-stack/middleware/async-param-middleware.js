"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = fn => async (req, res, next, id) => {
  try {
    await fn(req, res, id);
  } catch (err) {
    next(err);
    return;
  }
  next();
};