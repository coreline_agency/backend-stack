"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = fn => async (req, res, next) => {
  try {
    await fn(req, res);
  } catch (err) {
    next(err);
    return;
  }
  next();
};