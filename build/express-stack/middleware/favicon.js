'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _expressJsonapiErrorMiddleware = require('../../express-jsonapi-error-middleware');

const { NotFoundError } = _expressJsonapiErrorMiddleware.knownErrors;

// There's no favicon.ico by default on the server and those
// requests are ignored. If you need it favicon.ico, use
// https://www.npmjs.com/package/serve-favicon

exports.default = () => (req, res, next) => {
  if (req.originalUrl === '/favicon.ico') {
    return next(new NotFoundError());
  }

  return next();
};