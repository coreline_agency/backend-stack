'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = injectLocalIntoIo;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _expressIo = require('../../express-io');

var _expressIo2 = _interopRequireDefault(_expressIo);

var _locals = require('../locals');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function injectLocalIntoIo(localPath, ioPath) {
  return (req, res, next) => {
    const document = _expressIo2.default.get(req);
    const value = (0, _locals.getLocals)(req, localPath);

    if (document && value) {
      _lodash2.default.set(document, ioPath, value);
    }

    return next();
  };
}