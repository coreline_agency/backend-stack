'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildFilterQuery = buildFilterQuery;
exports.buildPageQuery = buildPageQuery;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const filterOperators = ['gt', 'lt'];
const pageParams = ['limit', 'offset'];

/**
 * Builds jsonapi query based on input filter
 * Input-output example:
 * INPUT: {
 *    filter1: value1,
 *    filter2: value2,
 *    gt: {
 *      filter3: greaterThanThis
 *    },
 *  }
 * OUTPUT: {
 *  filter[filter1]: value1,
 *  filter[filter2]: value2,
 *  filter[filter3][gt]: greaterThanThis,
 * }
 * @param filter filter object
 * @returns jsoanpi filter object
 */
function buildFilterQuery(filter) {
  const query = {};
  _lodash2.default.forOwn(filter, (value, filterParam) => {
    if (_lodash2.default.includes(filterOperators, filterParam)) {
      _lodash2.default.forOwn(value, (operationFilterValue, operationFilterParam) => {
        query[`filter[${operationFilterParam}][${filterParam}]`] = operationFilterValue;
      });
    } else if (!_lodash2.default.isObject(value)) {
      query[`filter[${filterParam}]`] = value;
    }
  });
  return query;
}

function buildPageQuery(page) {
  const query = {};
  _lodash2.default.forOwn(page, (value, pageParam) => {
    if (_lodash2.default.includes(pageParams, pageParam)) {
      query[`page[${pageParam}]`] = value;
    }
  });
}

exports.default = {
  buildFilterQuery,
  buildPageQuery
};