'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.requireEnvString = requireEnvString;
exports.requireEnvNumber = requireEnvNumber;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function requireEnvString(variableName, defaultValue) {
  const value = _lodash2.default.get(process, `env.${variableName}`) || defaultValue;
  if (_lodash2.default.isUndefined(value)) {
    throw new Error(`Required env variable '${variableName}' is not defined`);
  }
  return value;
}

function requireEnvNumber(variableName, defaultValue) {
  const value = _lodash2.default.get(process, `env.${variableName}`) || defaultValue;
  if (_lodash2.default.isUndefined(value)) {
    throw new Error(`Required env variable '${variableName}' is not defined`);
  }
  const numericValue = parseFloat(value);
  if (isNaN(numericValue)) {
    throw new Error(`Env variable '${variableName}=${value}' is not numeric`);
  }
  return numericValue;
}