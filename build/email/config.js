'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _env = require('../env');

exports.default = {
  mandrillApiKey: (0, _env.requireEnvString)('MANDRILL_API_KEY')
};