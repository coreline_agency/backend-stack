'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sendTemplateMail = sendTemplateMail;
exports.sendMail = sendMail;

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _mandrill = require('mandrill-api/mandrill');

var _mandrill2 = _interopRequireDefault(_mandrill);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mandrillClient = new _mandrill2.default.Mandrill(_config2.default.mandrillApiKey);

function transformMergeVars(variables) {
  return _lodash2.default.map(variables, (value, key) => ({
    name: key,
    content: value
  }));
}

function validateResult(result) {
  if (_lodash2.default.some(result, r => _lodash2.default.includes(['rejected', 'invalid'], r.status))) {
    throw new Error(result);
  }
}

/**
 * Sends email with template content
 * @param {string} from Sender email address
 * @param {string} to Receiver email address
 * @param {string} templateName Name of mandrill mail template
 * @param {Object} templateVars Variables used in template
 * @returns {Promise} Result from mandrill
 */
async function sendTemplateMail(from, to, templateName, templateVars) {
  const message = {
    from_email: from,
    to: [{ email: to }],
    global_merge_vars: transformMergeVars(templateVars)
  };

  const emailOptions = {
    template_name: templateName,
    template_content: [],
    message,
    async: false
  };

  const result = await new _bluebird2.default((resolve, reject) => mandrillClient.messages.sendTemplate(emailOptions, resolve, reject));
  validateResult(result);
  return result;
}

/**
 * Sends email with custom content
 * @param {string} from Sender email address
 * @param {string} to Receiver email address
 * @param {string} subject Mail subject
 * @param {string} textContent Mail text content
 * @param {string} htmlContent Mail HTML content
 * @returns {Promise} Result from mandrill
 */
async function sendMail(from, to, subject, textContent, htmlContent) {
  const message = {
    from_email: from,
    to: [{ email: to }],
    subject,
    text: textContent,
    html: htmlContent
  };

  const emailOptions = {
    message,
    async: false
  };

  const result = await new _bluebird2.default((resolve, reject) => mandrillClient.messages.send(emailOptions, resolve, reject));
  validateResult(result);
  return result;
}