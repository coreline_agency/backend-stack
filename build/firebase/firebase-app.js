'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _firebaseAdmin = require('firebase-admin');

var _firebaseAdmin2 = _interopRequireDefault(_firebaseAdmin);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = _firebaseAdmin2.default.initializeApp({
  credential: _firebaseAdmin2.default.credential.cert({
    projectId: _config2.default.projectId,
    clientEmail: _config2.default.clientEmail,
    privateKey: _config2.default.privateKey
  })
});

exports.default = app;