'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _env = require('../env');

let privateKey = (0, _env.requireEnvString)('FIREBASE_PRIVATE_KEY', '');
if (!privateKey.length) {
  privateKey = new Buffer((0, _env.requireEnvString)('FIREBASE_PRIVATE_KEY_BASE64'), 'base64').toString('ascii');
}

exports.default = {
  projectId: (0, _env.requireEnvString)('FIREBASE_PROJECT_ID'),
  clientEmail: (0, _env.requireEnvString)('FIREBASE_CLIENT_EMAIL'),
  privateKey
};