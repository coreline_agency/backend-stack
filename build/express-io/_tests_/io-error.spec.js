'use strict';

var _chai = require('chai');

var _ioError = require('../io-error');

describe('IO error', () => {
  describe('error adapter', () => {
    it('ignores other errors', () => {
      const error = new Error('Some other error');
      _chai.assert.isNotOk(_ioError.ioErrorAdapter.toJsonApi(error));
    });

    it('handles IO errors', () => {
      const error = new _ioError.IoError('Io error');
      _chai.assert.isObject(_ioError.ioErrorAdapter.toJsonApi(error));
    });

    it('includes the information from the IO error', () => {
      const error = new _ioError.IoError('Io error');
      _chai.assert.deepEqual(_ioError.ioErrorAdapter.toJsonApi(error), {
        status: 400,
        title: 'Bad request',
        detail: error.message,
        meta: {
          trace: error
        }
      });
    });
  });
});