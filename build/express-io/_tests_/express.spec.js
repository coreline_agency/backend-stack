'use strict';

var _chai = require('chai');

var _nodeMocksHttp = require('node-mocks-http');

var _express = require('../express');

describe('getFilter', () => {
  it('returns correctly when filter is not set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const ret = (0, _express.getFilter)(req);
    _chai.assert.isUndefined(ret);
  });

  it('returns correct value when filter is set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const filter = { field: ['value1'] };

    (0, _express.setFilter)(req, filter);

    _chai.assert.deepEqual((0, _express.getFilter)(req), { field: ['value1'] });
  });
});

describe('setFilter', () => {
  it('sets filter correctly', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const filter = { field: ['value1', 'value2'] };

    (0, _express.setFilter)(req, filter);

    _chai.assert.deepEqual(req.locals.io.filter, { field: ['value1', 'value2'] });
  });
});

describe('getSort', () => {
  it('returns correctly when sort is not set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const ret = (0, _express.getSort)(req);
    _chai.assert.isUndefined(ret);
  });

  it('returns correct value when sort is set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const sort = 'name';

    (0, _express.setSort)(req, sort);

    _chai.assert.deepEqual((0, _express.getSort)(req), 'name');
  });
});

describe('setSort', () => {
  it('sets sort correctly', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const sort = 'name';

    (0, _express.setSort)(req, sort);

    _chai.assert.deepEqual(req.locals.io.sort, 'name');
  });
});

describe('getPage', () => {
  it('returns correctly when page is not set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const ret = (0, _express.getPage)(req);
    _chai.assert.isUndefined(ret);
  });

  it('returns correct value when page is set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const page = { limit: 1 };

    (0, _express.setPage)(req, page);

    _chai.assert.deepEqual((0, _express.getPage)(req), { limit: 1 });
  });
});

describe('setPage', () => {
  it('sets page correctly', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const page = { limit: 1 };

    (0, _express.setSort)(req, page);

    _chai.assert.deepEqual(req.locals.io.sort, { limit: 1 });
  });
});

describe('getMeta', () => {
  it('returns correctly when meta is not set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const ret = (0, _express.getMeta)(req);
    _chai.assert.isUndefined(ret);
  });

  it('returns correct value when meta is set', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const meta = { count: 123 };

    (0, _express.setMeta)(req, meta);

    _chai.assert.deepEqual((0, _express.getMeta)(req), { count: 123 });
  });
});

describe('setMeta', () => {
  it('sets meta correctly', () => {
    const req = (0, _nodeMocksHttp.createRequest)();
    const meta = { count: 123 };

    (0, _express.setMeta)(req, meta);

    _chai.assert.deepEqual(req.locals.io.meta, { count: 123 });
  });
});