'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.get = get;
exports.getStatus = getStatus;
exports.set = set;
exports.setCreated = setCreated;
exports.setEmpty = setEmpty;
exports.setFilter = setFilter;
exports.getFilter = getFilter;
exports.setSort = setSort;
exports.getSort = getSort;
exports.setPage = setPage;
exports.getPage = getPage;
exports.getPageOrDefault = getPageOrDefault;
exports.setMeta = setMeta;
exports.getMeta = getMeta;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _status = require('./status');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Returns the IO data from the given express object (req/res).
 *
 * @param target The express request or response.
 * @returns {*} The IO data.
 */
function get(target) {
  return _lodash2.default.get(target, 'locals.io.data');
}

/**
 * Returns the IO status from the given express object (req/res).
 *
 * @param target The express request or response.
 * @returns {string} The IO status.
 */
function getStatus(target) {
  // The default IO status is ok
  return _lodash2.default.get(target, 'locals.io.status', _status.Status.OK);
}

/**
 * Sets the IO object and status on the given express
 * object (req/res).
 *
 * @param target The express request or response.
 * @param data The IO data to set.
 * @param status The IO status to set, defaults to OK.
 */
function set(target, data, status = _status.Status.OK) {
  if (!target.locals) {
    target.locals = {};
  }

  if (!target.locals.io) {
    target.locals.io = {};
  }

  target.locals.io.data = data;
  target.locals.io.status = status;
}

/**
 * Sets the IO object with the status CREATED on the
 * given express object (req/res).
 *
 * @param target The express request or response.
 * @param data The IO data to set.
 */
function setCreated(target, data) {
  set(target, data, _status.Status.CREATED);
}

/**
 * Sets an empty response on the given express object (req/res).
 * This method will clear any existing IO data, and set the status
 * to NO_CONTENT.
 *
 * @param target The express request or response.
 */
function setEmpty(target) {
  set(target, null, _status.Status.NO_CONTENT);
}

/**
 * Sets a filter field in given object.
 *
 * @param target The express object given
 * @param filter The filter object to set
 */
function setFilter(target, filter) {
  if (!target.locals) {
    target.locals = {};
  }

  if (!target.locals.io) {
    target.locals.io = {};
  }

  target.locals.io.filter = filter;
}

/**
 * Returns filter field from given object
 *
 * @param target The express object
 * @returns {*} Filter field of given object
 */
function getFilter(target) {
  return _lodash2.default.get(target, 'locals.io.filter');
}

/**
 * Sets a sort field in given object.
 *
 * @param target The express object given
 * @param sort The sort object to set
 */
function setSort(target, sort) {
  if (!target.locals) {
    target.locals = {};
  }

  if (!target.locals.io) {
    target.locals.io = {};
  }

  target.locals.io.sort = sort;
}

/**
 * Returns sort field from given object
 *
 * @param target The express object
 * @returns {*} Sort field of given object
 */
function getSort(target) {
  return _lodash2.default.get(target, 'locals.io.sort');
}

/**
 * Sets a page field in given object.
 *
 * @param target The express object given
 * @param page The page object to set
 */
function setPage(target, page) {
  if (!target.locals) {
    target.locals = {};
  }

  if (!target.locals.io) {
    target.locals.io = {};
  }

  target.locals.io.page = page;
}

/**
 * Returns page field from given object
 *
 * @param target The express object
 * @returns {*} Page field of given object
 */
function getPage(target) {
  return _lodash2.default.get(target, 'locals.io.page');
}

/**
 * Returns page field from given object
 *
 * @param target The express object
 * @param defaultLimit Default limit used if not defined in request. Fallback for this default is 20.
 * @returns {*} Page field of given object
 */
function getPageOrDefault(target, defaultLimit = 20) {
  const page = getPage(target) || {};
  return _extends({
    offset: 0,
    limit: defaultLimit
  }, page);
}

/**
 * Sets a meta field in given object.
 *
 * @param target The express object given
 * @param meta The meta object to set
 */
function setMeta(target, meta) {
  if (!target.locals) {
    target.locals = {};
  }

  if (!target.locals.io) {
    target.locals.io = {};
  }

  target.locals.io.meta = meta;
}

/**
 * Returns meta field from given object
 *
 * @param target The express object
 * @returns {*} Meta field of given object
 */
function getMeta(target) {
  return _lodash2.default.get(target, 'locals.io.meta');
}