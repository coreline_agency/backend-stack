'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const Status = exports.Status = Object.freeze({
  OK: 'ok',
  CREATED: 'created',
  NO_CONTENT: 'no-content'
});