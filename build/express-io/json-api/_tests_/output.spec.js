// import URI from 'urijs';
// import { assert } from 'chai';
// import sinon from 'sinon';
// import { createRequest, createResponse } from 'node-mocks-http';
//
// import output, { Output } from '../output';
// import io from '../../index';
// import { getPage, setPage, setMeta } from '../../express';
//
// sinon.assert.expose(assert, { prefix: '' });
//
// describe('IO output flow', () => {
//   describe('prepareResponse200', () => {
//     it('sets status 200', () => {
//       const req = createRequest();
//       const res = createResponse();
//
//       output.prepareResponse200(req, res);
//       assert.strictEqual(res.statusCode, 200);
//     });
//
//     it('supports content serialization', () => {
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isTrue(output.prepareResponse200(req, res));
//     });
//   });
//
//   describe('prepareResponse201', () => {
//     it('throws an error if resource id is falsy', () => {
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.throws(() => output.prepareResponse201(req, res), 'resource has no id');
//     });
//
//     it('sets header "Location" and status 201', () => {
//       const req = createRequest({
//         url: 'http://appmanager.com/v1/apps/123/installations',
//       });
//       const res = createResponse();
//       const options = {
//         id: '_id',
//       };
//       io.set(res, { _id: '10000' });
//
//       output.prepareResponse201(req, res, options);
//       assert.strictEqual(res.get('Location'), '/v1/apps/123/installations/10000');
//       assert.strictEqual(res.statusCode, 201);
//     });
//
//     it('supports content serialization', () => {
//       const req = createRequest({
//         url: 'http://appmanager.com/v1/apps/123/installations',
//       });
//       const res = createResponse();
//       const options = {
//         id: '_id',
//       };
//       io.set(res, { _id: '10000' });
//
//       assert.isTrue(output.prepareResponse201(req, res, options));
//     });
//   });
//
//   describe('prepareResponse204', () => {
//     it('sets status 204', () => {
//       const req = createRequest();
//       const res = createResponse();
//
//       output.prepareResponse204(req, res);
//       assert.strictEqual(res.statusCode, 204);
//     });
//
//     it('does not support content serialization', () => {
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isFalse(output.prepareResponse204(req, res));
//     });
//   });
//
//   describe('setResponseHeaders', () => {
//     it('sets the JSON API content type', () => {
//       const res = createResponse();
//
//       output.setResponseHeaders(res);
//       assert.strictEqual(res.get('Content-Type'), 'application/vnd.api+json');
//     });
//   });
//
//   describe('prepareResponse', () => {
//     it('prepares the response for status CREATED', sinon.test(function () {
//       this.stub(output, 'prepareResponse201').returns(true);
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isTrue(output.prepareResponse(io.Status.CREATED, req, res));
//       assert.calledWith(output.prepareResponse201, req, res);
//     }));
//
//     it('prepares the response for status NO_CONTENT', sinon.test(function () {
//       this.stub(output, 'prepareResponse204').returns(false);
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isFalse(output.prepareResponse(io.Status.NO_CONTENT, req, res));
//       assert.calledWith(output.prepareResponse204, req, res);
//     }));
//
//     it('prepares the response for status OK', sinon.test(function () {
//       this.stub(output, 'prepareResponse200').returns(true);
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isTrue(output.prepareResponse(io.Status.OK, req, res));
//       assert.calledWith(output.prepareResponse200, req, res);
//     }));
//
//     it('prepares the response for unknown status', sinon.test(function () {
//       this.stub(output, 'prepareResponse200').returns(true);
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isTrue(output.prepareResponse('_UNKNOWN_', req, res));
//       assert.calledWith(output.prepareResponse200, req, res);
//     }));
//
//     it('prepares the response for undefined status', sinon.test(function () {
//       this.stub(output, 'prepareResponse200').returns(true);
//       const req = createRequest();
//       const res = createResponse();
//
//       assert.isTrue(output.prepareResponse(undefined, req, res));
//       assert.calledWith(output.prepareResponse200, req, res);
//     }));
//   });
//
//   describe('generateOutput', () => {
//     const typeName = 'test.type';
//
//     it('sets the correct response status', sinon.test(function () {
//       this.stub(output, 'prepareResponse').returns(true);
//       this.stub(output, 'setResponseHeaders');
//
//       const req = createRequest();
//       const res = createResponse();
//       const status = 'test-status';
//       io.set(res, null, status);
//
//       const next = sinon.spy();
//
//       output.generateOutput(typeName, {})(req, res, next);
//       assert.calledWith(output.prepareResponse, status, req, res);
//     }));
//
//     it('ends the request if there is no data to serialize', sinon.test(function () {
//       this.stub(output, 'prepareResponse').returns(false);
//       this.stub(output, 'setResponseHeaders');
//
//       const req = createRequest();
//       const res = createResponse();
//
//       const next = sinon.spy();
//
//       output.generateOutput(typeName, {})(req, res, next);
//       assert.isTrue(res._isEndCalled(), 'The response was not closed');
//       assert.notCalled(output.setResponseHeaders);
//     }));
//
//     it('sets the response headers', sinon.test(function () {
//       this.stub(output, 'prepareResponse').returns(true);
//       this.stub(output, 'setResponseHeaders');
//
//       const req = createRequest();
//       const res = createResponse();
//
//       const next = sinon.spy();
//
//       output.generateOutput(typeName, {})(req, res, next);
//       assert.calledWith(output.setResponseHeaders, res);
//     }));
//
//     it('serializes valid IO data', sinon.test(function () {
//       this.stub(output, 'prepareResponse').returns(true);
//       this.stub(output, 'setResponseHeaders');
//
//       const req = createRequest();
//       const res = createResponse();
//
//       const input = {
//         title: 'Some title',
//         relationship: {
//           id: 'relationshipId',
//           title: 'Relationship title',
//         },
//         _id: 'abcdef',
//         nestedObject: {
//           key: 'value',
//         },
//       };
//       io.set(res, input);
//
//       const relationshipType = 'test.relationships';
//       const serializationOptions = {
//         id: '_id',
//         keyForAttribute: 'camelCase',
//         attributes: [
//           'title',
//           'relationship',
//           'nestedObject',
//         ],
//         relationship: {
//           ref: 'id',
//           attributes: [
//             'title',
//           ],
//         },
//         typeForAttribute(attribute) {
//           switch (attribute) {
//             case 'relationship': return relationshipType;
//             default: return attribute;
//           }
//         },
//       };
//
//       const next = sinon.spy();
//       output.generateOutput(typeName, serializationOptions)(req, res, next);
//       assert(res._isJSON());
//
//       const payload = JSON.parse(res._getData());
//       assert.deepEqual(payload, {
//         data: {
//           type: typeName,
//           id: input._id,
//           attributes: {
//             title: input.title,
//             nestedObject: input.nestedObject,
//           },
//           relationships: {
//             relationship: {
//               data: {
//                 type: relationshipType,
//                 id: input.relationship.id,
//               },
//             },
//           },
//         },
//         included: [{
//           type: relationshipType,
//           id: input.relationship.id,
//           attributes: {
//             title: input.relationship.title,
//           },
//         }],
//       });
//       assert.isUndefined(payload.data.attributes._id, 'Object ID should not be in `attributes`');
//       assert.notCalled(next);
//     }));
//
//     it('calls the error handler if there is no data to serialize', sinon.test(function () {
//       this.stub(output, 'prepareResponse').returns(true);
//       this.stub(output, 'setResponseHeaders');
//
//       const req = createRequest();
//       const res = createResponse();
//       io.set(res, null, io.Status.OK);
//
//       const next = sinon.spy();
//
//       output.generateOutput(typeName, {})(req, res, next);
//       assert.calledWith(next, sinon.match.defined);
//       assert.include(next.getCall(0).args[0].message, 'no data to serialize');
//     }));
//
//     it('handles any errors during the serialization', sinon.test(function () {
//       const error = new Error('Serialization error');
//       this.stub(output, 'prepareResponse').throws(error);
//
//       const req = createRequest();
//       const res = createResponse();
//
//       const next = sinon.spy();
//
//       output.generateOutput(typeName, {})(req, res, next);
//       assert.calledWith(next, error);
//     }));
//   });
//
//
//   describe('setMeta', () => {
//     it('Sets meta options property from response', () => {
//       const meta = { count: 321 };
//       const res = {};
//       const options = {};
//
//       setMeta(res, meta);
//
//       const out = new Output();
//       out.setMeta(res, options);
//
//       assert.deepEqual(options.meta, meta);
//     });
//
//     it('Does nothing if meta does not exist in response', () => {
//       const res = {};
//       const options = {};
//
//       const out = new Output();
//       out.createPaginationLinks = sinon.stub().returns(options);
//       out.setMeta(res, options);
//
//       assert.isUndefined(options.meta);
//     });
//   });
//
//   describe('setPaginationLinks', () => {
//     function getMockData() {
//       const offset = 30;
//       const limit = 10;
//       const hasNext = false;
//
//       const req = {
//         protocol: 'http',
//         headers: {
//           host: 'www.nekaj.hr',
//         },
//         originalUrl: '/v3/neka/akcija?neki=4&parametar=blabla',
//       };
//
//       const res = {};
//       setPage(res, { offset, limit, hasNext });
//
//       return { offset, limit, hasNext, req, res };
//     }
//
//     it('Sets topLevelLinks to options object', () => {
//       const { offset, limit, hasNext, req, res } = getMockData();
//
//       const prev = 'http://www.abc.def/ghij/klmn';
//       const next = 'http://www.opr.stu/xyz';
//       const url = `${req.protocol}://${req.headers.host}${req.originalUrl}`;
//       const customLink = 'www.bla.custom.bla';
//
//       const options = { topLevelLinks: { customLink } };
//
//       const out = new Output();
//
//       out.createPaginationLinks = sinon.stub().returns({ prev, next });
//
//       const optionsWithLinks = out.setPaginationLinks(req, res, options);
//
//       assert.calledWith(out.createPaginationLinks, limit, offset, hasNext, url);
//       assert.equal(optionsWithLinks.topLevelLinks.prev, prev);
//       assert.equal(optionsWithLinks.topLevelLinks.next, next);
//       assert.equal(optionsWithLinks.topLevelLinks.customLink, customLink);
//     });
//
//     it('Will not set next and prev to topLevelLinks if there is no paging options in response',
//       () => {
//         const { req, res } = getMockData();
//         setPage(res, null);
//
//         let options = {};
//
//         const out = new Output();
//         options = out.setPaginationLinks(req, res, options);
//
//         assert.equal(options.topLevelLinks, undefined);
//       });
//
//     it('Will not set next and prev to topLevelLinks if offset is not set',
//       () => {
//         const { req, res } = getMockData();
//
//         const page = getPage(res);
//         page.offset = null;
//
//         let options = {};
//
//         const out = new Output();
//         options = out.setPaginationLinks(req, res, options);
//
//         assert.equal(options.topLevelLinks, undefined);
//       });
//
//     it('Will not set next and prev to topLevelLinks if limit is not set',
//       () => {
//         const { req, res } = getMockData();
//
//         const page = getPage(res);
//         page.limit = null;
//
//         let options = {};
//
//         const out = new Output();
//         options = out.setPaginationLinks(req, res, options);
//
//         assert.equal(options.topLevelLinks, undefined);
//       });
//
//     it('Will not set next and prev to topLevelLinks if hasNext is not set',
//       () => {
//         const { req, res } = getMockData();
//
//         const page = getPage(res);
//         page.hasNext = null;
//
//         let options = {};
//
//         const out = new Output();
//         options = out.setPaginationLinks(req, res, options);
//
//         assert.equal(options.topLevelLinks, undefined);
//       });
//   });
//
//   describe('createLinks', () => {
//     const limit = 10;
//     const offset = 30;
//     const hasNext = true;
//
//     const somethingrandom = 'trlababalan';
//     const baseUrl = `http://www.nekaj.com/bla/bla?somethingrandom=${somethingrandom}`;
//
//     it('Returns prev and next link', () => {
//       const out = new Output();
//
//       const { prev, next } = out.createPaginationLinks(limit, offset, hasNext, baseUrl);
//
//       const parsedPrevQs = new URI(prev).search(true);
//       const parsedNextQs = new URI(next).search(true);
//
//       assert.equal(parsedPrevQs['page[offset]'], 20);
//       assert.equal(parsedPrevQs['page[limit]'], 10);
//       assert.equal(parsedPrevQs.somethingrandom, somethingrandom);
//
//       assert.equal(parsedNextQs['page[offset]'], 40);
//       assert.equal(parsedNextQs['page[limit]'], 10);
//       assert.equal(parsedNextQs.somethingrandom, somethingrandom);
//     });
//
//     it('Prev link is null if offset equals zero', () => {
//       const out = new Output();
//
//       const { prev } = out.createPaginationLinks(limit, 0, hasNext, baseUrl);
//
//       assert.equal(prev, null);
//     });
//
//     it('Next link is null if hasNext is null', () => {
//       const out = new Output();
//
//       const { next } = out.createPaginationLinks(limit, offset, false, baseUrl);
//
//       assert.equal(next, null);
//     });
//   });
// });
"use strict";