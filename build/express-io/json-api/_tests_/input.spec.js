// import { assert } from 'chai';
// import sinon from 'sinon';
// import _ from 'lodash';
// import { createRequest, createResponse } from 'node-mocks-http';
//
// import input from '../input';
// import io from '../../index';
// import { IoError } from '../../io-error';
// import {
//   getFilter,
//   getSort,
//   getPage,
// } from '../../express';
//
// sinon.assert.expose(assert, { prefix: '' });
//
// const jsonApiContentType = 'application/vnd.api+json';
// const validTypeName = 'namespace.objects';
//
// describe('IO input flow', () => {
//   describe('validateRequestHeaders', () => {
//     it('throws an error if the `Accept` header is missing', () => {
//       const req = createRequest();
//       assert.throws(() => input.validateRequestHeaders(req),
//         'Client does not accept JSON API responses.'
//       );
//     });
//
//     it('throws an error if the `Accept` header does not allow JSON API responses', () => {
//       const req = createRequest({
//         headers: {
//           Accept: 'text/plain',
//         },
//       });
//       assert.throws(() => input.validateRequestHeaders(req),
//         'Client does not accept JSON API responses.'
//       );
//     });
//
//     it('does not throw an error if the client accepts JSON API responses', () => {
//       const req = createRequest({
//         headers: {
//           Accept: jsonApiContentType,
//         },
//       });
//       assert.doesNotThrow(() => input.validateRequestHeaders(req));
//     });
//
//     it('does not throw an error if the client accepts JSON API and other data types', () => {
//       const req = createRequest({
//         headers: {
//           Accept: `text/plain,${jsonApiContentType},*/*`,
//         },
//       });
//       assert.doesNotThrow(() => input.validateRequestHeaders(req));
//     });
//
//     it('does not throw an error if the client accepts all data types', () => {
//       const req = createRequest({
//         headers: {
//           Accept: '*/*',
//         },
//       });
//       assert.doesNotThrow(() => input.validateRequestHeaders(req));
//     });
//
//     it('does not throw an error if the client uses quality params in "Accept" header', () => {
//       const req = createRequest({
//         headers: {
//           Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
//         },
//       });
//       assert.doesNotThrow(() => input.validateRequestHeaders(req));
//     });
//   });
//
//   describe('hasJsonApiPayload', () => {
//     it('returns true for JSON API content type', () => {
//       const req = createRequest({
//         headers: {
//           'Content-Type': jsonApiContentType,
//         },
//       });
//       assert.isTrue(input.hasJsonApiPayload(req));
//     });
//
//     it('returns false for non JSON API content type', () => {
//       const req = createRequest({
//         headers: {
//           'Content-Type': 'text/html',
//         },
//       });
//       assert.isFalse(input.hasJsonApiPayload(req));
//     });
//
//     it('returns true for JSON API content type with charset utf8 media type parameter', () => {
//       const req = createRequest({
//         headers: {
//           'Content-Type': `${jsonApiContentType}; charset=utf-8`,
//         },
//       });
//       assert.isTrue(input.hasJsonApiPayload(req));
//     });
//
//     it('returns false for request without content type', () => {
//       const req = createRequest({
//         headers: {
//           'Content-Type': 'text/html',
//         },
//       });
//       assert.isFalse(input.hasJsonApiPayload(req));
//     });
//   });
//
//   describe('validateRequestBody', () => {
//     it('throws an error for missing `req.body.data`', sinon.test(function () {
//       const req = createRequest();
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       assert.throws(() => input.validateRequestBody(validTypeName, req), '"data" is missing');
//     }));
//
//     it('calls resource validation for `req.body.data` object', sinon.test(function () {
//       const resource = {
//         type: validTypeName,
//         id: 1,
//       };
//       const req = createRequest({
//         body: {
//           data: resource,
//         },
//       });
//       this.stub(input, 'validateResource');
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       input.validateRequestBody(validTypeName, req);
//       assert.calledWithExactly(input.validateResource, resource, validTypeName);
//     }));
//
//     it('calls resource validation for each object in `req.body.data` array',
//       sinon.test(function () {
//         const resources = [{
//           type: validTypeName,
//           id: 1,
//         }, {
//           type: validTypeName,
//           id: 2,
//         }, {
//           type: validTypeName,
//           id: 3,
//         }];
//         const req = createRequest({
//           body: {
//             data: resources,
//           },
//         });
//         this.stub(input, 'validateResource');
//         this.stub(input, 'hasJsonApiPayload').returns(true);
//
//         input.validateRequestBody(validTypeName, req);
//         _.each(resources, (resource, index) => {
//           assert.calledWithExactly(input.validateResource, resource, validTypeName, index);
//         });
//       }));
//
//     it('does not throw errors for requests without JSON API content', sinon.test(function () {
//       const req = createRequest({
//         body: '<html><head></head><body>Some text</body></html>',
//       });
//       this.stub(input, 'hasJsonApiPayload').returns(false);
//
//       assert.doesNotThrow(() => input.validateRequestBody(validTypeName, req));
//     }));
//   });
//
//   describe('validateResource', () => {
//     it('throws an error for missing `resource.type`', () => {
//       const resource = {};
//
//       assert.throws(() => input.validateResource(resource, validTypeName),
//         '"data.type" is missing');
//     });
//
//     it('throws an error with index of resource for missing `resource.type`',
//       () => {
//         const resource = {};
//         const index = 2;
//
//         assert.throws(() => input.validateResource(resource, validTypeName, index),
//           `"data[${index}].type" is missing`);
//       });
//
//     it('throws an error for wrong data type', sinon.test(function () {
//       const dataType = 'whatever';
//       const resource = {
//         type: dataType,
//         id: 1,
//       };
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       assert.throws(() => input.validateResource(resource, validTypeName),
//         `'${dataType}' is not supported`
//       );
//     }));
//
//     it('does not throw errors for valid resource', sinon.test(function () {
//       const resource = {
//         type: validTypeName,
//         id: 1,
//       };
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       assert.doesNotThrow(() => input.validateResource(resource, validTypeName));
//     }));
//   });
//
//   describe('validateRequest', () => {
//     it('validates both request headers and body', sinon.test(function () {
//       const req = createRequest();
//       this.stub(input, 'validateRequestHeaders');
//       this.stub(input, 'validateRequestBody');
//
//       input.validateRequest(validTypeName, req);
//       assert.calledWith(input.validateRequestHeaders, req);
//       assert.calledWith(input.validateRequestBody, validTypeName, req);
//     }));
//   });
//
//   describe('deserializeRequest', () => {
//     it('ignores the requests without JSON API payload', sinon.test(function () {
//       this.stub(input, 'hasJsonApiPayload').returns(false);
//
//       const req = createRequest();
//       const callback = sinon.spy();
//
//       input.deserializeRequest(null, req, callback);
//       assert.calledWithExactly(callback, null, null);
//     }));
//
//     it('deserializes a valid payload', sinon.test(function () {
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       const result = {
//         id: '343',
//         name: 'some name',
//       };
//       const req = createRequest({
//         body: {
//           data: {
//             type: validTypeName,
//             id: result.id,
//             attributes: {
//               name: result.name,
//             },
//           },
//         },
//       });
//
//       const deserializer = {
//         deserialize: sinon.stub().callsArgWith(1, null, result),
//       };
//       const callback = sinon.spy();
//
//       input.deserializeRequest(deserializer, req, callback);
//       assert.calledWithExactly(callback, null, result);
//     }));
//
//     it('calls a callback with error for invalid payload', sinon.test(function () {
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       const req = createRequest();
//       const error = new Error('Error while parsing data');
//       const deserializer = {
//         deserialize: sinon.stub().callsArgWith(1, error),
//       };
//       const callback = sinon.spy();
//
//       input.deserializeRequest(deserializer, req, callback);
//       assert.calledWithExactly(callback, error);
//     }));
//
//     it('calls a callback with error if deserializer throws an exception', sinon.test(function () {
//       this.stub(input, 'hasJsonApiPayload').returns(true);
//
//       const req = createRequest();
//       const error = new Error('Error while parsing data');
//       const deserializer = {
//         deserialize: sinon.stub().throws(error),
//       };
//       const callback = sinon.spy();
//
//       input.deserializeRequest(deserializer, req, callback);
//       assert.calledWithExactly(callback, error);
//     }));
//   });
//
//   describe('processInput', () => {
//     it('sets the deserialized IO data for valid requests', sinon.test(function () {
//       const result = {
//         id: '343',
//         name: 'some name',
//       };
//
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, result);
//
//       const req = createRequest();
//       const res = createResponse();
//       const next = sinon.spy();
//
//       input.processInput(validTypeName, {})(req, res, next);
//       assert.calledWithExactly(next);
//       assert.deepEqual(io.get(req), result);
//     }));
//
//     it('validates requests', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest');
//       const req = createRequest();
//
//       input.processInput(validTypeName, {})(req);
//       assert.calledWith(input.validateRequest, validTypeName, req);
//     }));
//
//     it('deserializes requests', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest');
//
//       const req = createRequest();
//       input.processInput(validTypeName, {})(req);
//       assert.calledWith(input.deserializeRequest, sinon.match.defined, req);
//     }));
//
//     it('raises an error for invalid requests', sinon.test(function () {
//       const error = new Error('Invalid request');
//       this.stub(input, 'validateRequest').throws(error);
//
//       const next = sinon.spy();
//       input.processInput(validTypeName, {})(null, null, next);
//       assert.calledWith(next, error);
//     }));
//
//     it('propagates deserialization errors', sinon.test(function () {
//       const error = new Error('Invalid request body');
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, error);
//
//       const next = sinon.spy();
//       input.processInput(validTypeName, {})(null, null, next);
//       assert.calledWith(next, error);
//     }));
//
//     it('correctly handles requests without JSON API payload', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const res = createResponse();
//       const next = sinon.spy();
//       input.processInput(validTypeName, {})(null, res, next);
//       assert.calledWithExactly(next);
//       assert.isUndefined(io.get(res));
//     }));
//
//     it('correctly handles requests without filter and sort query parameters',
//       sinon.test(function () {
//         this.stub(input, 'validateRequest');
//         this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//         const req = createRequest();
//         const res = createResponse();
//         const next = sinon.spy();
//
//         input.processInput(validTypeName, {})(req, res, next);
//         assert.calledWithExactly(next);
//       }));
//
//     it('uses wildcard filter when allowedFilterFileds is not set', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           filter: { field: 'value1,value2' },
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//
//       input.processInput(validTypeName, null)(req, res, next);
//
//       assert.calledWithExactly(next);
//       assert.deepEqual(getFilter(req), { field: ['value1', 'value2'] });
//     }));
//
//     it('correctly sets filter when allowedFilterFields is set', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           filter: { field: 'value1,value2' },
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//
//       input.processInput(validTypeName, { allowedFilterFields: ['field'] })(req, res, next);
//
//       assert.calledWithExactly(next);
//       assert.deepEqual(getFilter(req), { field: ['value1', 'value2'] });
//     }));
//
//     it('raises error when not allowed filter field is requested', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           filter: { field: 'value1,value2' },
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//       const error = new IoError('Filtering error: Filter field not allowed');
//
//       input.processInput(validTypeName, { allowedFilterFields: ['otherField'] })(req, res, next);
//
//       assert.calledWithExactly(next, error);
//     }));
//
//     it('uses wildcard sort when allowedSortFileds is not set', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'parseFilterQuery');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           sort: 'name',
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//
//       input.processInput(validTypeName, null)(req, res, next);
//
//       assert.calledWithExactly(next);
//       assert.deepEqual(getSort(req), 'name');
//     }));
//
//     it('correctly sets sort when allowedSortFields is set', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: { sort: 'name,id' },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//
//       input.processInput(validTypeName, { allowedSortFields: ['name', 'id'] })(req, res, next);
//
//       assert.calledWithExactly(next);
//       assert.deepEqual(getSort(req), ['name', 'id']);
//     }));
//
//     it('raises error when not allowed sort field is requested', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: { sort: 'name,err,id' },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//       const error = new IoError('Sorting error: Sort field not allowed');
//
//       input.processInput(validTypeName, { allowedSortFields: ['name', 'id'] })(req, res, next);
//
//       assert.calledWithExactly(next, error);
//     }));
//
//     it('correctly sets page when allowedPageFields is set', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           page: { offset: 1, limit: 2 },
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//
//       input.processInput(validTypeName, { allowedPageFields: ['limit', 'offset'] })(req, res, next);
//
//       assert.calledWithExactly(next);
//       assert.deepEqual(getPage(req), { offset: 1, limit: 2 });
//     }));
//
//     it('raises error when not allowed page field is requested', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           page: {
//             limit: 1,
//             err: 1,
//           },
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//       const error = new IoError('Pagination error: Page field not allowed');
//
//       input.processInput(validTypeName)(req, res, next);
//
//       assert.calledWithExactly(next, error);
//     }));
//
//     it('raises error when negative page is requested', sinon.test(function () {
//       this.stub(input, 'validateRequest');
//       this.stub(input, 'deserializeRequest').callsArgWith(2, null, null);
//
//       const req = {
//         query: {
//           page: {
//             limit: 1,
//             offset: -1,
//           },
//         },
//       };
//       const res = createResponse();
//       const next = sinon.spy();
//       const error = new IoError('Pagination error: Page field must be positive number or zero');
//
//       input.processInput(validTypeName, {})(req, res, next);
//
//       assert.calledWithExactly(next, error);
//     }));
//   });
// });
"use strict";