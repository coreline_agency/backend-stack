'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defaultOptions = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.setupIo = setupIo;

var _input = require('./input');

var _input2 = _interopRequireDefault(_input);

var _output = require('./output');

var _output2 = _interopRequireDefault(_output);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const defaultOptions = exports.defaultOptions = {
  keyForAttribute: 'camelCase'
};

/**
 * Initializes the io middleware framework that can be used to
 * parse and validate the API requests, and generate the API
 * responses for JSON API endpoints.
 *
 * @param type The name of the JSON API data type.
 * @param [outOptions] The serialization options.
 * @param [inOptions] The deserialization options.
 * @returns {*} An object with the io middleware factory methods.
 */
function setupIo(type, outOptions, inOptions) {
  if (!type) {
    throw new Error('The io type is a required value');
  }

  const resolvedInOptions = _extends({}, defaultOptions, inOptions);

  const resolvedOutOptions = _extends({}, defaultOptions, outOptions);
  return {
    /**
     * Returns a middleware function that parses and
     * validates incoming requests.
     *
     * @param [inOptions] Custom options that will be merged with
     *   the defaults provided in the setup function.
     * @returns {function()} The middleware function.
     */
    in(inOptions) {
      return _input2.default.processInput(type, _extends({}, resolvedInOptions, inOptions));
    },

    /**
     * Returns a middleware function that generates the
     * response data in the JSON API format.
     *
     * @param [outOptions] Custom options that will be merged with
     *   the defaults provided in the setup function.
     * @returns {function()} The middleware function.
     */
    out(outOptions) {
      return _output2.default.generateOutput(type, _extends({}, resolvedOutOptions, outOptions));
    }
  };
}