'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _jsonapiSerializer = require('jsonapi-serializer');

var _contentType = require('content-type');

var _contentType2 = _interopRequireDefault(_contentType);

var _express = require('../express');

var _ioError = require('../io-error');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const jsonApiContentType = 'application/vnd.api+json';
const supportedContentTypes = [jsonApiContentType, '*/*', '*'];

exports.default = {
  /*
   * Calls the error handler if client did not send the correct "Accept" header.
   */
  validateRequestHeaders(req) {
    const acceptHeader = req.get('Accept') || '';
    const contentTypes = [];
    // Remove (optional) quality value from each accepted type.
    // https://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.9
    for (const type of acceptHeader.split(',')) {
      contentTypes.push(type.split(';')[0]);
    }

    const acceptedContentTypes = _lodash2.default.intersection(contentTypes, supportedContentTypes);
    if (_lodash2.default.isEmpty(acceptedContentTypes)) {
      const message = 'Client does not accept JSON API responses. ' + 'Did you set the correct "Accept" header?';
      throw new _ioError.IoError(message);
    }
  },

  /**
   * Returns true if the request has a JSON API content.
   *
   * @param req The request.
   * @returns {boolean} true if the request has a JSON API content,
   *   false otherwise.
   */
  hasJsonApiPayload(req) {
    try {
      return _contentType2.default.parse(req.get('Content-Type')).type === jsonApiContentType;
    } catch (e) {
      return false;
    }
  },

  /*
   * Throws an error if request specifies JSON API content type, but:
   * - does not conform to JSON API spec, or
   * - specifies the wrong data type.
   */
  validateRequestBody(type, req) {
    if (!this.hasJsonApiPayload(req)) {
      // We only validate the requests with the JSON API body
      return;
    }
    if (!_lodash2.default.get(req.body, 'data')) {
      throw new _ioError.IoError('"data" is missing from request body');
    }

    if (_lodash2.default.isArray(req.body.data)) {
      _lodash2.default.each(req.body.data, (resource, index) => {
        this.validateResource(resource, type, index);
      });
    } else {
      this.validateResource(req.body.data, type);
    }
  },

  validateResource(resource, type, index) {
    const requiredPaths = ['type'];
    for (const path of requiredPaths) {
      if (!_lodash2.default.get(resource, path)) {
        if (_lodash2.default.isUndefined(index)) {
          throw new _ioError.IoError(`"data.${path}" is missing from request body`);
        } else {
          throw new _ioError.IoError(`"data[${index}].${path}" is missing from request body`);
        }
      }
    }

    const dataType = resource.type;
    if (dataType !== type) {
      const message = `This endpoint only works with "${type}", ` + `type '${dataType}' is not supported`;
      throw new _ioError.IoError(message);
    }
  },

  /*
   * Validates the request headers and body (if exists).
   */
  validateRequest(type, req) {
    this.validateRequestHeaders(req);
    this.validateRequestBody(type, req);
  },

  /**
   * Deserializes the JSON API request body.
   *
   * @param deserializer The deserializer to use.
   * @param req The incoming request.
   * @param callback The callback that will be called with the result.
   */
  deserializeRequest(deserializer, req, callback) {
    if (!this.hasJsonApiPayload(req)) {
      // Nothing to do here
      return callback(null, null);
    }

    try {
      return deserializer.deserialize(req.body, callback);
    } catch (err) {
      return callback(err);
    }
  },

  /**
   * Parses filter query from the request (if exists)
   *
   * @param req The incoming request
   * @param options The options passed from processInput function
   * @param callback The callback that will be called if filter exists.
   */
  parseFilterQuery(req, options, callback) {
    if (_lodash2.default.get(req, 'query.filter')) {
      const queryFilter = req.query.filter;
      // '*' is wildcard for all filter fields allowed
      const allowedFilterFields = _lodash2.default.get(options, 'allowedFilterFields') || '*';

      callback(req, queryFilter, allowedFilterFields);
    }
  },

  /**
   * Parses sort query from the request (if exists)
   *
   * @param req The incoming request
   * @param options The options passed from `processInput` function
   * @param callback The callback that will be called if sort exists
   */
  parseSortQuery(req, options, callback) {
    if (_lodash2.default.get(req, 'query.sort')) {
      const querySort = req.query.sort.split(',');
      // '*' is wildcard for all sort fields allowed
      const allowedSortFields = _lodash2.default.get(options, 'allowedSortFields') || '*';

      callback(req, querySort, allowedSortFields);
    }
  },

  /**
   * Parses page query from the request (if exists)
   *
   * @param req The incoming request
   * @param options The options passed from `processInput` function
   * @param callback The callback that will be called if page exists
   */
  parsePageQuery(req, callback) {
    if (_lodash2.default.get(req, 'query.page')) {
      const queryPage = req.query.page;

      if (_lodash2.default.has(queryPage, 'limit')) {
        _lodash2.default.set(queryPage, 'limit', _lodash2.default.toNumber(_lodash2.default.get(queryPage, 'limit')));
      }

      if (_lodash2.default.has(queryPage, 'offset')) {
        _lodash2.default.set(queryPage, 'offset', _lodash2.default.toNumber(_lodash2.default.get(queryPage, 'offset')));
      }

      callback(req, queryPage, ['limit', 'offset']);
    }
  },

  /**
   * Initializes a middleware that validates and parses the data from
   * the JSON API request. This middleware validates the request headers,
   * and body, if it exists. If the request is valid, the middleware will
   * also deserialize the request body and save it into `req.locals.io.data`
   * property.
   *
   * @param type The name of the JSON API data type.
   * @param options Deserialization options (https://github.com/SeyZ/jsonapi-serializer)
   * @returns {function()} The input middleware.
   */
  processInput(type, defaultOptions) {
    const deserializer = new _jsonapiSerializer.Deserializer(defaultOptions);

    return (req, res, next) => {
      const options = _lodash2.default.cloneDeep(defaultOptions);
      try {
        this.validateRequest(type, req);
        this.parseFilterQuery(req, options, (req, queryFilter, allowedFilterFields) => {
          const allFilterFieldsAllowed = allowedFilterFields === '*';
          const filter = {};

          Object.keys(queryFilter).forEach(field => {
            if (!allFilterFieldsAllowed && !allowedFilterFields.find(item => item === field)) {
              const message = 'Filtering error: Filter field not allowed';
              throw new _ioError.IoError(message);
            }

            if (_lodash2.default.includes(queryFilter[field], ',')) {
              filter[field] = queryFilter[field].split(',');
            } else {
              filter[field] = queryFilter[field] === 'null' ? null : queryFilter[field];
            }
          });

          (0, _express.setFilter)(req, filter);
        });
        this.parseSortQuery(req, options, (req, querySort, allowedSortFields) => {
          const allSortFieldsAllowed = allowedSortFields === '*';

          querySort.forEach(field => {
            if (!allSortFieldsAllowed && !allowedSortFields.find(item => item === field)) {
              const message = 'Sorting error: Sort field not allowed';
              throw new _ioError.IoError(message);
            }
          });

          if (querySort.length === 1) {
            (0, _express.setSort)(req, querySort[0]);
          } else {
            (0, _express.setSort)(req, querySort);
          }
        });
        this.parsePageQuery(req, (req, queryPage, allowedPageFields) => {
          Object.keys(queryPage).forEach(field => {
            if (!allowedPageFields.find(item => item === field)) {
              const message = 'Pagination error: Page field not allowed';
              throw new _ioError.IoError(message);
            }

            if (!_lodash2.default.isFinite(queryPage[field])) {
              const message = 'Pagination error: Page field must be a number';
              throw new _ioError.IoError(message);
            }

            if (queryPage[field] < 0) {
              const message = 'Pagination error: Page field must be positive number or zero';
              throw new _ioError.IoError(message);
            }
          });

          (0, _express.setPage)(req, queryPage);
        });

        return this.deserializeRequest(deserializer, req, (err, model) => {
          if (err) {
            return next(err);
          }

          if (model) {
            (0, _express.set)(req, model);
          }

          return next();
        });
      } catch (err) {
        return next(err);
      }
    };
  }
};