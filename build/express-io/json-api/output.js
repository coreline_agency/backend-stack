'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Output = undefined;

var _urijs = require('urijs');

var _urijs2 = _interopRequireDefault(_urijs);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _jsonapiSerializer = require('jsonapi-serializer');

var _express = require('../express');

var _ioError = require('../io-error');

var _status = require('../status');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let Output = exports.Output = class Output {
  /**
   * Sets response status to `200 OK`.
   *
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse200(req, res) {
    res.status(200);
    return true;
  }

  /**
   * Sets response `Location` header and status to `201 Created`.
   *
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse201(req, res, options) {
    const resource = (0, _express.get)(res);
    const idName = _lodash2.default.get(options, 'id', 'id');
    const resourceId = _lodash2.default.get(resource, idName);
    if (_lodash2.default.isUndefined(resourceId)) {
      const message = 'Failed to prepare response 201: resource has no id';
      throw new _ioError.IoError(message);
    }

    const location = new _urijs2.default(req.url).segment(String(resourceId)).path();

    res.set('Location', location);
    res.status(201);
    return true;
  }

  /**
   * Sets response status to `204 No Content`.
   *
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse204(req, res) {
    res.status(204);
    return false;
  }

  /**
   * Prepares the response for a given status.
   *
   * @param status The response status.
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse(status, req, res, options) {
    switch (status) {
      case _status.Status.CREATED:
        return this.prepareResponse201(req, res, options);
      case _status.Status.NO_CONTENT:
        return this.prepareResponse204(req, res);
      default:
        return this.prepareResponse200(req, res);
    }
  }

  /**
   * Create meta properties in jsonapi-serializer options
   *
   * @param res The response object.
   * @param options Serialization options (https://github.com/SeyZ/jsonapi-serializer)
   */
  setMeta(res, options) {
    const extendedOptions = options;

    const meta = (0, _express.getMeta)(res);
    if (meta) {
      if (!extendedOptions.meta) {
        extendedOptions.meta = {};
      }

      _lodash2.default.merge(extendedOptions.meta, meta);
    }

    return extendedOptions;
  }

  /**
   * Create topLevelLinks property for options object if request have paging options set
   *
   * @param req The request object.
   * @param res The response object.
   * @param options Serialization options (https://github.com/SeyZ/jsonapi-serializer)
   */
  setPaginationLinks(req, res, options) {
    const page = (0, _express.getPage)(res);
    if (!page) {
      return options;
    }
    page.offset = parseInt(page.offset, 10);
    page.limit = parseInt(page.limit, 10);

    if (!_lodash2.default.isFinite(page.offset) || !_lodash2.default.isFinite(page.limit) || _lodash2.default.isNil(page.hasNext)) {
      return options;
    }

    const url = `${req.protocol}://${req.headers.host}${req.originalUrl}`;

    const { prev, next } = this.createPaginationLinks(page.limit, page.offset, page.hasNext, url);

    if (!options.topLevelLinks) {
      options.topLevelLinks = {};
    }

    options.topLevelLinks.prev = prev;
    options.topLevelLinks.next = next;

    return options;
  }

  /**
   * Returns prev and next link for topLevelLinks options property
   *
   * @param limit Page size.
   * @param offset Number of objects skiped by pagination
   * @param hasNext Does next page exist
   * @param url Complete request URL with query string
   */
  createPaginationLinks(limit, offset, hasNext, url) {
    let prev = null;
    let next = null;

    const uri = new _urijs2.default(url);

    if (offset > 0) {
      uri.setQuery('page[offset]', offset - limit);
      uri.setQuery('page[limit]', limit);

      prev = uri.toString();
    }

    if (hasNext.toString().toLowerCase() === 'true') {
      uri.setQuery('page[offset]', offset + limit);
      uri.setQuery('page[limit]', limit);

      next = uri.toString();
    }

    return { prev, next };
  }

  /**
   * Sets the Content-Type specified by JSON API spec.
   *
   * @param res The response object.
   */
  setResponseHeaders(res) {
    res.set('Content-Type', 'application/vnd.api+json');
  }

  /**
   * Initializes a middleware that can generate the JSON API
   * compliant response. This method uses the data from
   * `res.locals.io` object. Currently used values in that object are:
   * - data: contains the models that will be serialized in the response
   * - status: specifies the response status details/flow, can be one of:
   *   - created: returns the 201 Created response
   *   - no-content: returns the 204 No Content response
   *   - (all other cases): returns the 200 Ok response
   *
   * @param type The name of the JSON API data type.
   * @param options Serialization options (https://github.com/SeyZ/jsonapi-serializer)
   * @returns {function()} The output middleware.
   */
  generateOutput(type, defaultOptions) {
    return (req, res, next) => {
      const options = _lodash2.default.cloneDeep(defaultOptions);
      try {
        const data = (0, _express.get)(res);
        const status = (0, _express.getStatus)(res);
        const shouldSerializeData = this.prepareResponse(status, req, res, options);
        if (!shouldSerializeData) {
          res.end();
          return null;
        }

        this.setResponseHeaders(res);
        if (!data) {
          const message = 'Serialization failed: no data to serialize';
          return next(new _ioError.IoError(message));
        }

        let extendedOptions = this.setPaginationLinks(req, res, options);
        extendedOptions = this.setMeta(res, options);

        const serializer = new _jsonapiSerializer.Serializer(type, extendedOptions);
        res.json(serializer.serialize(data));
        return null;
      } catch (err) {
        return next(err);
      }
    };
  }
};
exports.default = new Output();