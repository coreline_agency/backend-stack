'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jsonapi = exports.ioErrorAdapter = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _express = require('./express');

var io = _interopRequireWildcard(_express);

var _status = require('./status');

var _ioError = require('./io-error');

var _index = require('./json-api/index.js');

var jsonapi = _interopRequireWildcard(_index);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

exports.ioErrorAdapter = _ioError.ioErrorAdapter;
exports.jsonapi = jsonapi;
exports.default = _extends({
  Status: _status.Status
}, io);