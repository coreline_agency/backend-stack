'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CrudSequelizeRepository = undefined;

var _crudSequelizeRepository = require('./crud-sequelize-repository');

exports.CrudSequelizeRepository = _crudSequelizeRepository.CrudSequelizeRepository;