'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CrudSequelizeRepository = undefined;

var _desc, _value, _class;

var _leanDecorator = require('../db/leanDecorator');

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

/**
 * Repository providing basic CRUD operations for Sequelize model.
 * Optional features like include can be turned on and customized via options.
 */
let CrudSequelizeRepository = exports.CrudSequelizeRepository = (_class = class CrudSequelizeRepository {
  /**
   * Constructor
   * @param Model Sequelize data model
   * @param options Object which can contain:
   *  - 'include' array of objects, include option description:
   *  http://docs.sequelizejs.com/manual/tutorial/models-usage.html#top-level-where-with-eagerly-loaded-models
   */
  constructor(Model, options = {}) {
    this.Model = Model;
    this.options = options;

    this.getAll = this.getAll.bind(this);
    this.get = this.get.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.remove = this.remove.bind(this);
  }

  async getAll() {
    return this.Model.findAll({ include: this.options.include });
  }

  async get(id) {
    return this.Model.findOne({ where: { id }, include: this.options.include });
  }

  async create(data) {
    const createdModel = await this.Model.create(data);
    return this.get(createdModel.id);
  }

  async update(id, data) {
    await this.Model.update(data, { where: { id } });
    return this.get(id);
  }

  remove(id) {
    return this.Model.destroy({ where: { id } });
  }
}, (_applyDecoratedDescriptor(_class.prototype, 'getAll', [_leanDecorator.lean], Object.getOwnPropertyDescriptor(_class.prototype, 'getAll'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'get', [_leanDecorator.lean], Object.getOwnPropertyDescriptor(_class.prototype, 'get'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'create', [_leanDecorator.lean], Object.getOwnPropertyDescriptor(_class.prototype, 'create'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'update', [_leanDecorator.lean], Object.getOwnPropertyDescriptor(_class.prototype, 'update'), _class.prototype)), _class);