'use strict';

var _chai = require('chai');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _locals = require('../locals');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('Modify request/response locals', () => {
  describe('setLocals', () => {
    it('adds arbitrary data to object even if it has no property `locals`', () => {
      const obj = {};
      (0, _locals.setLocals)(obj, 'test', { x: 10, y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('adds arbitrary data to `obj.locals[namespace]`', () => {
      const obj = { locals: {} };
      (0, _locals.setLocals)(obj, 'test', { x: 10, y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('extends data already present in `obj.locals[namespace]`', () => {
      const obj = { locals: { test: { x: 10 } } };
      (0, _locals.setLocals)(obj, 'test', { y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.test.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('does not affect other namespaces', () => {
      const obj = { locals: { other: { x: 10 } } };
      (0, _locals.setLocals)(obj, 'test', { y: 20 });
      _chai.assert.isOk(obj.locals && obj.locals.other && obj.locals.test);
      _chai.assert.strictEqual(obj.locals.other.x, 10);
      _chai.assert.strictEqual(obj.locals.test.y, 20);
    });

    it('replaces array with the new one', () => {
      const obj = { locals: { array: [{ x: 10 }, { y: 20 }] } };
      (0, _locals.setLocals)(obj, 'array', [{ y: 2 }]);
      _chai.assert.isOk(obj.locals && obj.locals.array);
      const x = _lodash2.default.find(obj.locals.array, 'x');
      const y = _lodash2.default.find(obj.locals.array, 'y');
      _chai.assert.isNotOk(x);
      _chai.assert.deepEqual(y, { y: 2 });
    });

    it('replaces array in sub object with the new one', () => {
      const obj = { locals: { someField: { array: [{ x: 10 }, { y: 20 }] } } };
      (0, _locals.setLocals)(obj, 'someField.array', [{ y: 2 }]);
      _chai.assert.isOk(obj.locals && obj.locals.someField);
      const x = _lodash2.default.find(obj.locals.someField.array, 'x');
      const y = _lodash2.default.find(obj.locals.someField.array, 'y');
      _chai.assert.isNotOk(x);
      _chai.assert.deepEqual(y, { y: 2 });
    });

    it('setting property to undefined value unsets the property', () => {
      const obj = { locals: { test: { x: 10 } } };
      (0, _locals.setLocals)(obj, 'test');
      _chai.assert.isUndefined(obj.locals.test);
    });
  });

  describe('getLocals', () => {
    it('returns `undefined` if any path is missing', () => {
      const obj1 = {};
      const obj2 = { locals: null };
      _chai.assert.isUndefined((0, _locals.getLocals)(obj1, 'test'));
      _chai.assert.isUndefined((0, _locals.getLocals)(obj2, 'test'));
    });

    it('returns data from `obj.locals[namespace]`', () => {
      const obj = { locals: { test: { x: 10, y: 20 } } };
      const result = (0, _locals.getLocals)(obj, 'test');
      _chai.assert.isOk(result);
      _chai.assert.strictEqual(result.x, 10);
      _chai.assert.strictEqual(result.y, 20);
    });
  });
});