'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _logger = require('../logger');

var _logger2 = _interopRequireDefault(_logger);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = () => (req, res, next) => {
  try {
    _logger2.default.info({ req });
  } catch (e) {
    console.error('Error while logging request', e); // eslint-disable-line no-console
  } finally {
    next();
  }
};