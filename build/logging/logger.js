'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bunyan = require('bunyan');

var _bunyan2 = _interopRequireDefault(_bunyan);

var _logging = require('./config/logging');

var _logging2 = _interopRequireDefault(_logging);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LOG_LEVELS = ['trace', 'debug', 'info', 'warn', 'error', 'fatal'];

const serializers = {
  err: _bunyan2.default.stdSerializers.err,
  req: _bunyan2.default.stdSerializers.req,
  res: _bunyan2.default.stdSerializers.res
};

function getLogLevel(level) {
  const checkedLevel = level.toLowerCase();
  if (LOG_LEVELS.indexOf(checkedLevel) === -1) {
    throw new Error(`${level} is not a valid log level and should be one of ${LOG_LEVELS}`);
  }
  return checkedLevel;
}

const logger = _bunyan2.default.createLogger({
  name: 'backend-stack-logger',
  serializers,
  streams: [{
    level: getLogLevel(_logging2.default.logLevel),
    stream: process.stdout
  }]
});

exports.default = logger;