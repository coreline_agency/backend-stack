'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _env = require('../../env');

const DEFAULT_LOG_LEVEL = 'info';

exports.default = {
  logLevel: (0, _env.requireEnvString)('LOG_LEVEL', DEFAULT_LOG_LEVEL)
};