'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logRequest = exports.logger = undefined;

var _logger = require('./logger');

var _logger2 = _interopRequireDefault(_logger);

var _logRequest = require('./middleware/log-request');

var _logRequest2 = _interopRequireDefault(_logRequest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.logger = _logger2.default;
exports.logRequest = _logRequest2.default;