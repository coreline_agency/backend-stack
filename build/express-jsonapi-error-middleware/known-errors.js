'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const DEFAULT_STATUS_CODE = '500';
let NodeError = exports.NodeError = class NodeError extends Error {
  constructor(detail = '', title = 'node error', code) {
    super(detail);
    this.title = title;
    this.detail = detail;
    this.code = code;
    Error.captureStackTrace(this, this.constructor);
  }
};

/*
Base http error that has status
 */

let HttpError = exports.HttpError = class HttpError extends NodeError {
  constructor(status = DEFAULT_STATUS_CODE, detail = '', title = 'http error', code) {
    super(detail, title, code);
    this.status = status && status.toString() || DEFAULT_STATUS_CODE;
  }
};
/*
 Thrown whenever user is not authorized.
 */

let NotAuthorizedError = exports.NotAuthorizedError = class NotAuthorizedError extends HttpError {
  constructor(detail = '', code = 'authorization_default') {
    super('401', detail, 'not authorized', code);
  }
};

/*
 Thrown whenever user is not authorized to access a resource.
 */

let ForbiddenError = exports.ForbiddenError = class ForbiddenError extends HttpError {
  constructor(detail = '', code = 'forbidden_default') {
    super('403', detail, 'forbidden', code);
  }
};

/*
 Thrown whenever a resource cannot be found.
 */

let NotFoundError = exports.NotFoundError = class NotFoundError extends HttpError {
  constructor(detail = '', code = 'notFound_default') {
    super('404', detail, 'not found', code);
  }
};

/*
 Thrown when incoming request data is malformed.
 */

let ValidationError = exports.ValidationError = class ValidationError extends HttpError {
  constructor(detail = '', code = 'validation_default') {
    super('400', detail, 'validation error', code);
  }
};

/*
 Thrown if issues occur during data (de)serialization.
 */

let IoError = exports.IoError = class IoError extends HttpError {
  constructor(detail = '', httpStatusCode, code = 'io_default') {
    super(httpStatusCode, detail, 'io error', code);
  }
};

/*
 Thrown if issues occur during id decoding.
 */

let InvalidIdError = exports.InvalidIdError = class InvalidIdError extends HttpError {
  constructor(detail = '', code = 'invalidId_default') {
    super('400', detail, 'invalid id', code);
  }
};

/*
 Used when an unexpected error occurs during api call.
 */

let ApiError = exports.ApiError = class ApiError extends HttpError {
  constructor(response, detail = 'Unexpected response while performing API call.', code = 'api_default') {
    super(DEFAULT_STATUS_CODE, detail, 'api error', code);
    // `response` also contains full request data.
    this.response = response;
  }
};