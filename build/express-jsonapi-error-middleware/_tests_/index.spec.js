'use strict';

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require('sinon-chai');

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _index = require('../index');

var _knownErrors = require('../known-errors');

var allKnownErrors = _interopRequireWildcard(_knownErrors);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.use(_sinonChai2.default);
const expect = _chai2.default.expect;

function prepareResponse() {
  return {
    json: _sinon2.default.spy(),
    status: _sinon2.default.stub().returnsThis()
  };
}

describe('errorHandler', () => {
  it('exposes known errors', () => {
    expect(_index.knownErrors).to.be.an('object').and.to.be.ok.and.to.deep.equal(allKnownErrors);
    for (const key of Object.keys(_index.knownErrors)) {
      const err = new _index.knownErrors[key]();

      expect(err).to.have.property('title').that.is.a('string');
      expect(err).to.have.property('detail').that.is.a('string');
      expect(err).to.have.property('stack').that.is.a('string');

      if (err instanceof _index.knownErrors.HttpError) {
        expect(err).to.have.property('status').that.is.a('string');
      }
    }
  });

  it('exposes default not found handler', done => {
    expect(_index.defaultNotFound).to.be.a('function');

    const res = {
      headersSent: false
    };
    (0, _index.defaultNotFound)(null, res, err => {
      try {
        _chai.assert.instanceOf(err, allKnownErrors.NotFoundError);
        done();
      } catch (err) {
        done(err);
      }
    });
  });

  it('exposes default not found handler which calls `next` without error if headers sent', () => {
    expect(_index.defaultNotFound).to.be.a('function');

    const res = {
      headersSent: true
    };
    const next = _sinon2.default.spy();
    (0, _index.defaultNotFound)(null, res, next);
    _chai.assert.calledOnce(next);
  });

  it('returns 500 in case of any exceptions', () => {
    const res = prepareResponse();
    const errorThrowingAdapter = {
      toJsonApi: _sinon2.default.stub().throws(new Error('test'))
    };
    const handler = (0, _index.errorHandler)({ customAdapters: [errorThrowingAdapter] });
    const next = _sinon2.default.spy();

    handler(null, null, res, next);

    const errorsOut = {
      errors: [{
        title: 'server error',
        status: '500'
      }]
    };
    expect(res.status).to.have.been.calledWith(500);
    expect(res.json).to.have.been.calledWithExactly(errorsOut);
    expect(next).to.have.been.calledWithExactly(errorsOut);
  });

  // it('returns an error in case of any errors that are known', () => {
  //   Object.keys(knownErrors).forEach((errorKey) => {
  //     const res = prepareResponse();
  //     const err = new knownErrors[errorKey]('test');
  //     const handler = errorHandler({ showFullError: true });
  //     const next = sinon.spy();
  //     handler(err, null, res, next);
  //
  //     expect(res.status).to.have.been.calledWith(parseInt(err.status, 10));
  //     const expectedErrors = {
  //       errors: [
  //         {
  //           detail: err.detail,
  //           title: err.title,
  //           status: err.status,
  //           meta: {
  //             trace: err,
  //           },
  //         },
  //       ],
  //     };
  //     if (err instanceof knownErrors.ApiError) {
  //       expectedErrors.errors[0].meta.trace = _.omit(err, ['status', 'detail', 'title']);
  //     }
  //     expect(res.json).to.have.been.calledWithExactly(expectedErrors);
  //     expect(next).to.have.been.calledWithExactly(expectedErrors);
  //   });
  // });

  it('it uses default adapters, and responds', () => {
    const req = null;
    const next = _sinon2.default.spy();
    const res = prepareResponse();
    const handler = (0, _index.errorHandler)({
      showFullError: true
    });
    const err = new Error('hello, world of errors');
    const expectedError = {
      title: 'server error',
      status: '500',
      meta: {
        trace: {
          detail: err.message,
          title: err.name,
          stack: err.stack
        }
      }
    };

    handler(err, req, res, next);

    expect(res.status).to.have.been.calledWith(500);
    expect(res.json).to.have.been.calledWithExactly({ errors: [expectedError] });
    expect(next).to.have.been.calledWithExactly({ errors: [expectedError] });
  });

  it('it uses the given settings to handle the error', () => {
    let MyError = class MyError {
      constructor(message) {
        this.message = message;
      }
    };

    const expectedError = {
      status: '987',
      title: 'my error'
    };
    const req = null;
    const next = _sinon2.default.spy();
    const res = prepareResponse();
    const handler = (0, _index.errorHandler)({
      customAdapters: [{
        toJsonApi: err => err instanceof MyError ? {
          status: '987',
          title: 'my error',
          detail: 'some error detail which will be removed'
        } : null
      }]
    });
    const err = new MyError('msg');

    handler(err, req, res, next);

    expect(res.status).to.have.been.calledWith(987);
    expect(res.json).to.have.been.calledWithExactly({ errors: [expectedError] });
    expect(next).to.have.been.calledWithExactly({ errors: [expectedError] });
  });
});