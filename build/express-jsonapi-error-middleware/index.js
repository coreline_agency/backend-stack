'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.knownErrors = undefined;
exports.defaultNotFound = defaultNotFound;
exports.errorHandler = errorHandler;

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _unknownErrorAdapter = require('./adapters/unknown-error-adapter');

var _knownErrorAdapter = require('./adapters/known-error-adapter');

var _serverErrorAdapter = require('./adapters/server-error-adapter');

var _apiErrorAdapter = require('./adapters/api-error-adapter');

var _knownErrors = require('./known-errors');

var allKnownErrors = _interopRequireWildcard(_knownErrors);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/*
 exposing known errors for usage.
 */
const knownErrors = exports.knownErrors = allKnownErrors;

/*
 A 404 response handler if routes are missed but only if the response did not set any headers.
 */
function defaultNotFound(req, res, next) {
  // send 404 only if response has not been prepared by other middleware
  if (res.headersSent) {
    next();
  } else {
    next(new allKnownErrors.NotFoundError());
  }
}

function errorHandler(settings = { showFullError: false }) {
  const adapters = settings.customAdapters || [];

  adapters.push(_apiErrorAdapter.apiErrorAdapter);
  adapters.push(_knownErrorAdapter.knownErrorAdapter);
  adapters.push(_serverErrorAdapter.serverErrorAdapter);
  adapters.push(_unknownErrorAdapter.unknownErrorAdapter);
  return (err, req, res, next) => {
    try {
      let i = 0;
      let error = null;
      do {
        error = adapters[i++].toJsonApi(err);
      } while (!error);

      // According to JSON API spec, error response always contains an array
      // of errors.
      const errors = _.isArray(error) ? error : [error];
      const status = parseInt(errors[0].status, 10);
      if (!settings.showFullError) {
        for (const e of errors) {
          delete e.detail;
          delete e.meta;
        }
      }
      res.status(status).json({ errors });
      next({ errors });
    } catch (exc) {
      const errors = {
        errors: [{
          title: 'server error',
          status: '500'
        }]
      };
      if (settings.showFullError) {
        errors.errors[0].meta = {
          trace: exc
        };
      }
      res.status(500).json(errors);
      next(errors);
    }
  };
}