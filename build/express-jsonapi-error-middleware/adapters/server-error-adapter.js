'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.serverErrorAdapter = undefined;

var _verror = require('verror');

const serverErrorAdapter = exports.serverErrorAdapter = {
  toJsonApi: err => {
    if (err instanceof _verror.VError || err instanceof _verror.WError) {
      return {
        status: '500',
        title: 'server error',
        detail: err.message,
        meta: {
          trace: err.toString()
        }
      };
    }
    return null;
  }
};