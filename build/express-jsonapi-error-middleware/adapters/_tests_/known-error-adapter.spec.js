'use strict';

var _chai = require('chai');

var _knownErrorAdapter = require('../known-error-adapter');

var _index = require('../../index');

describe('known-error-adapter', () => {
  describe('knownErrorAdapter', () => {
    it('passes over errors of String type', () => {
      const errorIn = new Error('whatever');
      const error = _knownErrorAdapter.knownErrorAdapter.toJsonApi(errorIn);
      (0, _chai.expect)(error).to.be.null; // eslint-disable-line no-unused-expressions
    });

    it('handles instances of known errors', () => {
      for (const errType of Object.keys(_index.knownErrors)) {
        const errorIn = new _index.knownErrors[errType]();
        const errorOut = {
          status: errorIn.status,
          title: errorIn.title,
          detail: errorIn.detail,
          meta: {
            trace: errorIn
          }
        };

        const error = _knownErrorAdapter.knownErrorAdapter.toJsonApi(errorIn);
        delete error.code;
        (0, _chai.expect)(error).to.deep.equal(errorOut);
      }
    });
  });
});