'use strict';

var _chai = require('chai');

var _verror = require('verror');

var _serverErrorAdapter = require('../server-error-adapter');

describe('server-error-adapter', () => {
  describe('serverErrorAdapter', () => {
    it('passes down instances of Error', () => {
      // eslint-disable-next-line no-unused-expressions
      (0, _chai.expect)(_serverErrorAdapter.serverErrorAdapter.toJsonApi(new Error())).to.be.null;
    });

    it('passes null for instances of String', () => {
      // eslint-disable-next-line no-unused-expressions
      (0, _chai.expect)(_serverErrorAdapter.serverErrorAdapter.toJsonApi('error!')).to.be.null;
    });

    it('handles VErrors', () => {
      const errorIn = new _verror.VError(new Error('Original error'), 'This is the error wrapper');
      const errorOut = {
        status: '500',
        title: 'server error',
        detail: 'This is the error wrapper: Original error',
        meta: {
          trace: 'VError: This is the error wrapper: Original error'
        }
      };

      const error = _serverErrorAdapter.serverErrorAdapter.toJsonApi(errorIn);
      (0, _chai.expect)(error).to.deep.equal(errorOut);
    });

    it('handles WErrors', () => {
      const err1 = new Error('Issue 1');
      const err2 = new _verror.WError(err1, 'Issue 2');
      const errorIn = new _verror.WError(err2, 'Main issue');
      const errorOut = {
        status: '500',
        title: 'server error',
        detail: 'Main issue',
        meta: {
          trace: 'WError: Main issue; caused by WError: Issue 2; caused by Error: Issue 1'
        }
      };

      const error = _serverErrorAdapter.serverErrorAdapter.toJsonApi(errorIn);
      (0, _chai.expect)(error).to.deep.equal(errorOut);
    });
  });
});