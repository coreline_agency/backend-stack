'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.apiErrorAdapter = undefined;

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _knownErrors = require('../known-errors');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function getStatusCode(response) {
  let status = _.get(response, 'statusCode');
  if (!status) {
    status = _.get(response, 'body.http_status_code');
  }
  return status || 500;
}
const apiErrorAdapter = exports.apiErrorAdapter = {
  toJsonApi: err => {
    if (err instanceof _knownErrors.ApiError) {
      return {
        status: getStatusCode(err.response).toString(),
        title: 'api error',
        detail: err.detail,
        code: err.code,
        meta: {
          trace: err.response ? _.omit(err, ['status', 'detail', 'title']) : null
        }
      };
    }
    // must return null so other adapters can try and recognize err
    return null;
  }
};