'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.knownErrorAdapter = undefined;

var _knownErrors = require('../known-errors');

var knownErrors = _interopRequireWildcard(_knownErrors);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

const ERROR_KEYS = Object.keys(knownErrors);
const knownErrorAdapter = exports.knownErrorAdapter = {
  toJsonApi: err => {
    let error = null;
    for (const errorKey of ERROR_KEYS) {
      if (err instanceof knownErrors[errorKey]) {
        error = {
          status: err.status,
          title: err.title,
          detail: err.detail,
          code: err.code,
          meta: {
            trace: err
          }
        };
        break;
      }
    }
    return error;
  }
};