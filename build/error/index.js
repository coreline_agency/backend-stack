'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _errorCodeGenerator = require('./error-code-generator');

Object.defineProperty(exports, 'generateErrorCode', {
  enumerable: true,
  get: function () {
    return _errorCodeGenerator.generateErrorCode;
  }
});