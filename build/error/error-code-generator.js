"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateErrorCode = generateErrorCode;
function generateErrorCode(module, type, errorId) {
  return `${module}_${type}_${errorId}`;
}