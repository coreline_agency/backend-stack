'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.assertAuthenticatedUser = exports.loadAuthenticatedUser = exports.authenticate = undefined;

var _authenticate = require('./middleware/authenticate');

var _authenticate2 = _interopRequireDefault(_authenticate);

var _loadAuthenticatedUser = require('./middleware/load-authenticated-user');

var _loadAuthenticatedUser2 = _interopRequireDefault(_loadAuthenticatedUser);

var _assertAuthenticatedUser = require('./middleware/assert-authenticated-user');

var _assertAuthenticatedUser2 = _interopRequireDefault(_assertAuthenticatedUser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.authenticate = _authenticate2.default;
exports.loadAuthenticatedUser = _loadAuthenticatedUser2.default;
exports.assertAuthenticatedUser = _assertAuthenticatedUser2.default;