'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _expressStack = require('../../express-stack');

var _expressJsonapiErrorMiddleware = require('../../express-jsonapi-error-middleware');

var _firebaseApp = require('../../firebase/firebase-app');

var _firebaseApp2 = _interopRequireDefault(_firebaseApp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = () => (0, _expressStack.asyncMiddleware)(async req => {
  let token;
  if (req.headers && req.headers.authorization) {
    const parts = req.headers.authorization.split(' ');
    const scheme = parts[0];
    const credentials = parts[1] || '';
    if (/^Bearer$/i.test(scheme)) {
      token = credentials;
    }
  }
  if (!token) {
    return;
  }

  try {
    const decodedToken = await _firebaseApp2.default.auth().verifyIdToken(token);
    (0, _expressStack.setLocals)(req, 'auth.token', decodedToken);
  } catch (err) {
    throw new _expressJsonapiErrorMiddleware.knownErrors.NotAuthorizedError();
  }
});