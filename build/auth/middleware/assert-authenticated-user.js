'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _expressStack = require('../../express-stack');

var _expressJsonapiErrorMiddleware = require('../../express-jsonapi-error-middleware');

exports.default = () => (0, _expressStack.asyncMiddleware)(async req => {
  const user = (0, _expressStack.getLocals)(req, 'auth.user');
  if (!user) {
    throw new _expressJsonapiErrorMiddleware.knownErrors.NotAuthorizedError();
  }
});