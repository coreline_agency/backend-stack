'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _expressStack = require('../../express-stack');

var _firebaseApp = require('../../firebase/firebase-app');

var _firebaseApp2 = _interopRequireDefault(_firebaseApp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = () => (0, _expressStack.asyncMiddleware)(async req => {
  const uid = (0, _expressStack.getLocals)(req, 'auth.token.uid');
  if (uid) {
    const user = await _firebaseApp2.default.auth().getUser(uid);
    (0, _expressStack.setLocals)(req, 'auth.user', user);
  }
});