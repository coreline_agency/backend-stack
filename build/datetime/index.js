'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createDbRange = createDbRange;
exports.createMomentRange = createMomentRange;

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('moment-timezone');

var _momentRange = require('moment-range');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const moment = (0, _momentRange.extendMoment)(_moment2.default);

function createDbRange(startsAt, endsAt, timeZoneId) {
  return [moment.tz(startsAt, timeZoneId).toISOString(), moment.tz(endsAt, timeZoneId).toISOString()];
}

function createMomentRange(startsAt, endsAt, timeZoneId) {
  return moment.range(moment.tz(startsAt, timeZoneId), moment.tz(endsAt, timeZoneId));
}