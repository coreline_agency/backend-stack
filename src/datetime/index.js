import Moment from 'moment';
import 'moment-timezone';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);

export function createDbRange(startsAt, endsAt, timeZoneId) {
  return [
    moment.tz(startsAt, timeZoneId).toISOString(),
    moment.tz(endsAt, timeZoneId).toISOString(),
  ];
}

export function createMomentRange(startsAt, endsAt, timeZoneId) {
  return moment.range(moment.tz(startsAt, timeZoneId), moment.tz(endsAt, timeZoneId));
}
