import { requireEnvString } from '../../env';

export default {
  options: {
    swaggerDefinition: {
      info: {
        title: 'Swagger API',
        version: '1.0.0',
      },
      host: requireEnvString('SWAGGER_BASE_URL', 'localhost:3000'),
      basePath: '/',
      securityDefinitions: {
        token: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization',
          description: 'Authorization header with value: "Bearer {token}", where token is provided by firebase auth',
        },
      },
      security: {
        token: [],
      },
      consumes: [
        'application/vnd.api+json',
      ],
      produces: [
        'application/vnd.api+json',
      ],
      schemes: ['http', 'https'],
    },
    apis: ['./src/**/*.js'],
  },
};
