import _ from 'lodash';

export function requireEnvString(variableName, defaultValue) {
  const value = _.get(process, `env.${variableName}`) || defaultValue;
  if (_.isUndefined(value)) {
    throw new Error(`Required env variable '${variableName}' is not defined`);
  }
  return value;
}

export function requireEnvNumber(variableName, defaultValue) {
  const value = _.get(process, `env.${variableName}`) || defaultValue;
  if (_.isUndefined(value)) {
    throw new Error(`Required env variable '${variableName}' is not defined`);
  }
  const numericValue = parseFloat(value);
  if (isNaN(numericValue)) {
    throw new Error(`Env variable '${variableName}=${value}' is not numeric`);
  }
  return numericValue;
}
