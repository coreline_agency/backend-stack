import {
  getLocals,
  setLocals,
} from './service';

export {
  getLocals,
  setLocals,
};
