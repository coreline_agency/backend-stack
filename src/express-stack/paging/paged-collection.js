import _ from 'lodash';

export default class PagedCollection {
  constructor(pageItems, pageInfo) {
    this._pageItems = pageItems;
    this._pageInfo = pageInfo;
  }

  static createFromNPlus1Result(resultItems, requestedPage) {
    const hasNext = resultItems.length > requestedPage.limit;
    const items = _.take(resultItems, requestedPage.limit);
    return new PagedCollection(items, { ...requestedPage, hasNext });
  }

  [Symbol.iterator]() {
    let index = -1;
    const pageItems = this._pageItems;

    return {
      next: () => ({ value: pageItems[++index], done: !(index in pageItems) }),
    };
  }

  getPageInfo() {
    return this._pageInfo;
  }

  getPageItems() {
    return this._pageItems;
  }

  setPageItems(items) {
    this._pageItems = items;
  }
}
