import {
  cacheDecoratorLegacy,
  cacheDecorator,
  invalidateCacheDecorator,
  invalidateCacheDecoratorLegacy,
} from './cache-decorators';

export {
  cacheDecorator,
  cacheDecoratorLegacy,
  invalidateCacheDecorator,
  invalidateCacheDecoratorLegacy,
};

