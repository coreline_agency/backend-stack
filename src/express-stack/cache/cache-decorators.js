/* eslint prefer-rest-params:0 */
import _ from 'lodash';

/**
 * Method for saving new data in cache and updating dependency keys for invalidation keys
 * @param cacheKey id of recorder element in cache
 * @param invalidationKeys array of invalidation keys for written element in cache
 * @param cacheProvider promisified cache, it must have methods: getAsync, setAsync and delAsync
 * @returns {Promise.<*>}
 */
async function updateInvalidationKeys(cacheKey, invalidationKeys, cacheProvider) {
  for (let i = 0; i < invalidationKeys.length; i++) {
    const invalidationKey = invalidationKeys[i];
    const dependencyKeys = await cacheProvider.getAsync(invalidationKey);
    let currentInvalidationKeysValues = [];

    if (!_.isNil(dependencyKeys)) {
      if (dependencyKeys.indexOf(cacheKey) === -1) {
        dependencyKeys.push(cacheKey);
        currentInvalidationKeysValues = dependencyKeys;
      } else {
        currentInvalidationKeysValues = dependencyKeys;
      }
    } else {
      currentInvalidationKeysValues.push(cacheKey);
    }
    await cacheProvider.setAsync(invalidationKey, currentInvalidationKeysValues);
  }
}

/**
 *
 * @param decoratorCallback decorator callback to be called after fetching data from database
 * @returns {Function} callback
 */
function fetch(decoratorCallback) {
  return function (err, databaseData) {
    decoratorCallback(err, databaseData);
  };
}

/**
 * Decorator for method which uses cache. This decorator is for method which uses callback as parameter.
 * For using this decorator you need to call it above the method on which you want to use decorator.
 * Example of usage:
 *        @cacheDecoratorLegacy((id) => `${constants.DEVELOPER_USER_ID}:${id}`,
 *        (userId) => [`${constants.DEVELOPER_INVALIDATION_KEY}:${userId}`,
 *        `${constants.APPLICATION_INVALIDATION_KEY}:${userId}`])
 * @param cacheKeyGenerator Function for generating key for entry in cache
 * @param cacheInvalidationKeyGenerator function which returns array of invalidation keys
 * @param cacheProvider promisified cache, it must have methods: getAsync, setAsync and delAsync
 * @returns {Function} Function for handling communication with cache
 */
function cacheDecoratorLegacy(cacheKeyGenerator, cacheInvalidationKeyGenerator, cacheProvider) {
  return function (target, name, descriptor) {
    const fetcher = descriptor.value;
    const getFromCache = async function () {
      const argumentsArray = Array.from(arguments);
      const keyGeneratorArgs = argumentsArray.splice(0, arguments.length - 1);
      const cacheKey = cacheKeyGenerator.apply(null, keyGeneratorArgs);
      const invalidationKeys = cacheInvalidationKeyGenerator.apply(null, keyGeneratorArgs);
      const repositoryCallback = arguments[arguments.length - 1];
      const scopeArguments = arguments;
      const fetcherClass = this;
      try {
        const result = await cacheProvider.getAsync(cacheKey);
        if (!_.isNil(result)) {
          repositoryCallback(null, result);
        } else {
          const decoratorCallback = async function (err, databaseData) {
            if (err) {
              repositoryCallback(err, null);
            }
            cacheProvider.setAsync(cacheKey, databaseData);
            await updateInvalidationKeys(cacheKey, invalidationKeys, cacheProvider);
            repositoryCallback(null, databaseData);
          };

          scopeArguments[scopeArguments.length - 1] = fetch(decoratorCallback);
          fetcher.apply(fetcherClass, scopeArguments);
        }
      } catch (err) {
        repositoryCallback(err, null);
      }
    };
    descriptor.value = getFromCache;

    return descriptor;
  };
}

/**
 * Decorator for method which uses cache. This decorator is for method which uses promises
 * For using this decorator you need to call it above the method on which you want to use decorator.
 * Example of usage:
 *        @cacheDecorator((id) => `${constants.DEVELOPER_USER_ID}:${id}`,
 *        (userId) => [`${constants.DEVELOPER_INVALIDATION_KEY}:${userId}`,
 *        `${constants.APPLICATION_INVALIDATION_KEY}:${userId}`])
 * @param cacheKeyGenerator Function for generating key for entry in cache
 * @param cacheInvalidationKeyGenerator function which returns array of invalidation keys
 * @param cacheProvider promisified cache, it must have methods: getAsync, setAsync and delAsync
 * @returns {Function} Function for handling communication with cache
 */
function cacheDecorator(cacheKeyGenerator, cacheInvalidationKeyGenerator, cacheProvider) {
  return function (target, name, descriptor) {
    const fetcher = descriptor.value;
    const getFromCache = async function () {
      const keyGeneratorArgs = Array.from(arguments);
      const cacheKey = cacheKeyGenerator.apply(null, keyGeneratorArgs);
      const invalidationKeys = cacheInvalidationKeyGenerator.apply(null, keyGeneratorArgs);
      const scopeArguments = arguments;
      const fetcherClass = this;
      const result = await cacheProvider.getAsync(cacheKey);
      if (!_.isNil(result)) {
        return result;
      }
      const databaseData = await fetcher.apply(fetcherClass, scopeArguments);
      cacheProvider.setAsync(cacheKey, databaseData);
      await updateInvalidationKeys(cacheKey, invalidationKeys, cacheProvider);
      return databaseData;
    };
    descriptor.value = getFromCache;

    return descriptor;
  };
}

/**
 * Decorator for cache invalidation. This decorator is for method which uses callback as parameter
 * For using this decorator you need to call it above the method on which you want to use decorator.
 * Example of usage:
 *        @invalidateCacheLegacy((userId) => [`${constants.DEVELOPER_INVALIDATION_KEY}:${userId}`,
 *        `${constants.APPLICATION_INVALIDATION_KEY}:${userId}`])
 * @param cacheInvalidationKeyGenerator function which returns array of invalidation keys
 * @param cacheProvider promisified cache, it must have methods: getAsync, setAsync and delAsync
 * @returns {Function} Function for cache invalidation
 */
function invalidateCacheDecoratorLegacy(cacheInvalidationKeyGenerator, cacheProvider) {
  return function (target, name, descriptor) {
    const databaseOperation = descriptor.value;
    const cacheInvalidation = async function () {
      const repositoryCallback = arguments[arguments.length - 1];
      const fetcherClass = this;
      const argumentsArray = Array.from(arguments);
      const keyGeneratorArgs = argumentsArray.splice(0, arguments.length - 1);
      const invalidationKeys = cacheInvalidationKeyGenerator.apply(null, keyGeneratorArgs);
      const scopeArguments = arguments;
      try {
        for (let i = 0; i < invalidationKeys.length; i++) {
          const invalidationKey = invalidationKeys[i];
          const dependencyKeys = await cacheProvider.getAsync(invalidationKey);
          if (!_.isNil(dependencyKeys)) {
            for (let j = 0; j < dependencyKeys.length; j++) {
              const dependencyKey = dependencyKeys[j];
              await cacheProvider.delAsync(dependencyKey);
            }
            await cacheProvider.delAsync(invalidationKey);
          }
        }
        databaseOperation.apply(fetcherClass, scopeArguments);
      } catch (err) {
        repositoryCallback(err, null);
      }
    };
    descriptor.value = cacheInvalidation;

    return descriptor;
  };
}

/**
 * Decorator for cache invalidation. This decorator is for method which uses promises
 * For using this decorator you need to call it above the method on which you want to use decorator.
 * Example of usage:
 *        @invalidateCache((userId) => [`${constants.DEVELOPER_INVALIDATION_KEY}:${userId}`,
 *        `${constants.APPLICATION_INVALIDATION_KEY}:${userId}`])
 * @param cacheInvalidationKeyGenerator function which returns array of invalidation keys
 * @param cacheProvider promisified cache, it must have methods: getAsync, setAsync and delAsync
 * @returns {Function} Function for cache invalidation
 */
function invalidateCacheDecorator(cacheInvalidationKeyGenerator, cacheProvider) {
  return function (target, name, descriptor) {
    const databaseOperation = descriptor.value;
    const cacheInvalidation = async function () {
      const fetcherClass = this;
      const keyGeneratorArgs = Array.from(arguments);
      const invalidationKeys = cacheInvalidationKeyGenerator.apply(null, keyGeneratorArgs);
      const scopeArguments = arguments;
      for (let i = 0; i < invalidationKeys.length; i++) {
        const invalidationKey = invalidationKeys[i];
        const dependencyKeys = await cacheProvider.getAsync(invalidationKey);
        if (!_.isNil(dependencyKeys)) {
          for (let j = 0; j < dependencyKeys.length; j++) {
            const dependencyKey = dependencyKeys[j];
            await cacheProvider.delAsync(dependencyKey);
          }
          await cacheProvider.delAsync(invalidationKey);
        }
      }
      return databaseOperation.apply(fetcherClass, scopeArguments);
    };
    descriptor.value = cacheInvalidation;

    return descriptor;
  };
}

export {
  cacheDecorator,
  cacheDecoratorLegacy,
  invalidateCacheDecorator,
  invalidateCacheDecoratorLegacy,
};
