export default fn => async (req, res, next) => {
  try {
    await fn(req, res);
  } catch (err) {
    next(err);
    return;
  }
  next();
};
