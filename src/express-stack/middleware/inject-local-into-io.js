import _ from 'lodash';
import io from '../../express-io';
import { getLocals } from '../locals';

export default function injectLocalIntoIo(localPath, ioPath) {
  return (req, res, next) => {
    const document = io.get(req);
    const value = getLocals(req, localPath);

    if (document && value) {
      _.set(document, ioPath, value);
    }

    return next();
  };
}
