import _ from 'lodash';
const filterOperators = ['gt', 'lt'];
const pageParams = ['limit', 'offset'];

/**
 * Builds jsonapi query based on input filter
 * Input-output example:
 * INPUT: {
 *    filter1: value1,
 *    filter2: value2,
 *    gt: {
 *      filter3: greaterThanThis
 *    },
 *  }
 * OUTPUT: {
 *  filter[filter1]: value1,
 *  filter[filter2]: value2,
 *  filter[filter3][gt]: greaterThanThis,
 * }
 * @param filter filter object
 * @returns jsoanpi filter object
 */
export function buildFilterQuery(filter) {
  const query = {};
  _.forOwn(filter, (value, filterParam) => {
    if (_.includes(filterOperators, filterParam)) {
      _.forOwn(value, (operationFilterValue, operationFilterParam) => {
        query[`filter[${operationFilterParam}][${filterParam}]`] = operationFilterValue;
      });
    } else if (!_.isObject(value)) {
      query[`filter[${filterParam}]`] = value;
    }
  });
  return query;
}

export function buildPageQuery(page) {
  const query = {};
  _.forOwn(page, (value, pageParam) => {
    if (_.includes(pageParams, pageParam)) {
      query[`page[${pageParam}]`] = value;
    }
  });
}

export default {
  buildFilterQuery,
  buildPageQuery,
};
