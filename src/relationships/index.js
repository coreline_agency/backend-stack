import _ from 'lodash';

export function mapRelationships(data, relationships) {
  const newData = _.cloneDeep(data);

  _.forEach(relationships, relationship => {
    if (!_.isUndefined(_.get(newData, relationship))) {
      _.set(newData, `${relationship}Id`, _.get(newData, relationship));
      _.unset(newData, relationship);
    }
  });

  return newData;
}
