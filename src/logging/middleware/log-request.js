import logger from '../logger';

export default () => (req, res, next) => {
  try {
    logger.info({ req });
  } catch (e) {
    console.error('Error while logging request', e); // eslint-disable-line no-console
  } finally {
    next();
  }
};
