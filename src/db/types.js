import pg from 'pg';

const timestampOID = 1114;

export class PgTypes {
  initialize() {
    const types = pg.types;

    types.setTypeParser(timestampOID, (stringValue) => stringValue.replace(' ', 'T'));
  }
}

export default new PgTypes();
