import _ from 'lodash';

const ioSequelizeFilterMapping = {
  gt: '$gt',
  lt: '$lt',
};

export default function (ioFilter) {
  const sequelizeFilter = {};
  _.each(ioFilter, (filterValue, filter) => {
    if (_.isObject(filterValue)) {
      _.each(filterValue, (value, operator) => {
        const sequelizeOperator = ioSequelizeFilterMapping[operator];
        if (sequelizeOperator) {
          sequelizeFilter[filter] = { [`${sequelizeOperator}`]: value };
        }
      });
    } else {
      sequelizeFilter[filter] = filterValue;
    }
  });
  return sequelizeFilter;
}
