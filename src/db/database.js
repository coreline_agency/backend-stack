import Sequelize from 'sequelize';
import config from './config/db';
import { requireEnvString } from '../env';
import { logger } from '../logging';

export class Database {
  /**
   *
   * @param dialect Database instance type. Defaults to postgres (possible: mysql,sqlite,postgres,mssql)
   */
  constructor(dialect = 'postgres') {
    this.sequelize = new Sequelize({
      username: requireEnvString('DB_USERNAME'),
      password: requireEnvString('DB_PASSWORD'),
      database: requireEnvString('DB_NAME'),
      dialect,
      protocol: dialect,
      host: requireEnvString('DB_HOST'),
      port: requireEnvString('DB_PORT', '5432'),
      operatorsAliases: config.operatorsAliases,
    });
  }

  connect() {
    return this.sequelize
      .authenticate()
      .then(() => {
        logger.info('Database connection has been established successfully.');
      })
      .catch(err => {
        logger.error(err, 'Unable to connect to the database');
      });
  }

  getInstance() {
    return this.sequelize;
  }
}

export default new Database();
