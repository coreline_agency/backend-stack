import config from './config';
import Promise from 'bluebird';
import mandrill from 'mandrill-api/mandrill';
import _ from 'lodash';
const mandrillClient = new mandrill.Mandrill(config.mandrillApiKey);

function transformMergeVars(variables) {
  return _.map(variables, (value, key) => ({
    name: key,
    content: value,
  }));
}

function validateResult(result) {
  if (_.some(result, r => _.includes(['rejected', 'invalid'], r.status))) {
    throw new Error(result);
  }
}

/**
 * Sends email with template content
 * @param {string} from Sender email address
 * @param {string} to Receiver email address
 * @param {string} templateName Name of mandrill mail template
 * @param {Object} templateVars Variables used in template
 * @returns {Promise} Result from mandrill
 */
export async function sendTemplateMail(from, to, templateName, templateVars) {
  const message = {
    from_email: from,
    to: [{ email: to }],
    global_merge_vars: transformMergeVars(templateVars),
  };

  const emailOptions = {
    template_name: templateName,
    template_content: [],
    message,
    async: false,
  };

  const result = await new Promise((resolve, reject) =>
    mandrillClient.messages.sendTemplate(emailOptions, resolve, reject));
  validateResult(result);
  return result;
}

/**
 * Sends email with custom content
 * @param {string} from Sender email address
 * @param {string} to Receiver email address
 * @param {string} subject Mail subject
 * @param {string} textContent Mail text content
 * @param {string} htmlContent Mail HTML content
 * @returns {Promise} Result from mandrill
 */
export async function sendMail(from, to, subject, textContent, htmlContent) {
  const message = {
    from_email: from,
    to: [{ email: to }],
    subject,
    text: textContent,
    html: htmlContent,
  };

  const emailOptions = {
    message,
    async: false,
  };

  const result = await new Promise((resolve, reject) =>
    mandrillClient.messages.send(emailOptions, resolve, reject));
  validateResult(result);
  return result;
}
