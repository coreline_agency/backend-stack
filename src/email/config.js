import { requireEnvString } from '../env';

export default {
  mandrillApiKey: requireEnvString('MANDRILL_API_KEY'),
};
