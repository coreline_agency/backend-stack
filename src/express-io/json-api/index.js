import input from './input';
import output from './output';

export const defaultOptions = {
  keyForAttribute: 'camelCase',
};

/**
 * Initializes the io middleware framework that can be used to
 * parse and validate the API requests, and generate the API
 * responses for JSON API endpoints.
 *
 * @param type The name of the JSON API data type.
 * @param [outOptions] The serialization options.
 * @param [inOptions] The deserialization options.
 * @returns {*} An object with the io middleware factory methods.
 */
export function setupIo(type, outOptions, inOptions) {
  if (!type) {
    throw new Error('The io type is a required value');
  }

  const resolvedInOptions = {
    ...defaultOptions,
    ...inOptions,
  };

  const resolvedOutOptions = {
    ...defaultOptions,
    ...outOptions,
  };
  return {
    /**
     * Returns a middleware function that parses and
     * validates incoming requests.
     *
     * @param [inOptions] Custom options that will be merged with
     *   the defaults provided in the setup function.
     * @returns {function()} The middleware function.
     */
    in(inOptions) {
      return input.processInput(type, {
        ...resolvedInOptions,
        ...inOptions,
      });
    },

    /**
     * Returns a middleware function that generates the
     * response data in the JSON API format.
     *
     * @param [outOptions] Custom options that will be merged with
     *   the defaults provided in the setup function.
     * @returns {function()} The middleware function.
     */
    out(outOptions) {
      return output.generateOutput(type, {
        ...resolvedOutOptions,
        ...outOptions,
      });
    },
  };
}
