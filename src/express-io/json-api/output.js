import URI from 'urijs';
import _ from 'lodash';
import { Serializer } from 'jsonapi-serializer';

import { get, getPage, getStatus, getMeta } from '../express';
import { IoError } from '../io-error';
import { Status } from '../status';

export class Output {
  /**
   * Sets response status to `200 OK`.
   *
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse200(req, res) {
    res.status(200);
    return true;
  }

  /**
   * Sets response `Location` header and status to `201 Created`.
   *
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse201(req, res, options) {
    const resource = get(res);
    const idName = _.get(options, 'id', 'id');
    const resourceId = _.get(resource, idName);
    if (_.isUndefined(resourceId)) {
      const message = 'Failed to prepare response 201: resource has no id';
      throw new IoError(message);
    }

    const location = new URI(req.url)
      .segment(String(resourceId))
      .path();

    res.set('Location', location);
    res.status(201);
    return true;
  }

  /**
   * Sets response status to `204 No Content`.
   *
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse204(req, res) {
    res.status(204);
    return false;
  }

  /**
   * Prepares the response for a given status.
   *
   * @param status The response status.
   * @param req The request object.
   * @param res The response object.
   * @returns {boolean} true if the request body should be
   *   serialized, false otherwise.
   */
  prepareResponse(status, req, res, options) {
    switch (status) {
      case Status.CREATED:
        return this.prepareResponse201(req, res, options);
      case Status.NO_CONTENT:
        return this.prepareResponse204(req, res);
      default:
        return this.prepareResponse200(req, res);
    }
  }

  /**
   * Create meta properties in jsonapi-serializer options
   *
   * @param res The response object.
   * @param options Serialization options (https://github.com/SeyZ/jsonapi-serializer)
   */
  setMeta(res, options) {
    const extendedOptions = options;

    const meta = getMeta(res);
    if (meta) {
      if (!extendedOptions.meta) {
        extendedOptions.meta = {};
      }

      _.merge(extendedOptions.meta, meta);
    }

    return extendedOptions;
  }

  /**
   * Create topLevelLinks property for options object if request have paging options set
   *
   * @param req The request object.
   * @param res The response object.
   * @param options Serialization options (https://github.com/SeyZ/jsonapi-serializer)
   */
  setPaginationLinks(req, res, options) {
    const page = getPage(res);
    if (!page) {
      return options;
    }
    page.offset = parseInt(page.offset, 10);
    page.limit = parseInt(page.limit, 10);

    if (!_.isFinite(page.offset) || !_.isFinite(page.limit) ||
      _.isNil(page.hasNext)) {
      return options;
    }

    const url = `${req.protocol}://${req.headers.host}${req.originalUrl}`;

    const { prev, next } = this.createPaginationLinks(
      page.limit, page.offset, page.hasNext, url);

    if (!options.topLevelLinks) {
      options.topLevelLinks = {};
    }

    options.topLevelLinks.prev = prev;
    options.topLevelLinks.next = next;

    return options;
  }

  /**
   * Returns prev and next link for topLevelLinks options property
   *
   * @param limit Page size.
   * @param offset Number of objects skiped by pagination
   * @param hasNext Does next page exist
   * @param url Complete request URL with query string
   */
  createPaginationLinks(limit, offset, hasNext, url) {
    let prev = null;
    let next = null;

    const uri = new URI(url);

    if (offset > 0) {
      uri.setQuery('page[offset]', offset - limit);
      uri.setQuery('page[limit]', limit);

      prev = uri.toString();
    }

    if (hasNext.toString().toLowerCase() === 'true') {
      uri.setQuery('page[offset]', offset + limit);
      uri.setQuery('page[limit]', limit);

      next = uri.toString();
    }

    return { prev, next };
  }

  /**
   * Sets the Content-Type specified by JSON API spec.
   *
   * @param res The response object.
   */
  setResponseHeaders(res) {
    res.set('Content-Type', 'application/vnd.api+json');
  }

  /**
   * Initializes a middleware that can generate the JSON API
   * compliant response. This method uses the data from
   * `res.locals.io` object. Currently used values in that object are:
   * - data: contains the models that will be serialized in the response
   * - status: specifies the response status details/flow, can be one of:
   *   - created: returns the 201 Created response
   *   - no-content: returns the 204 No Content response
   *   - (all other cases): returns the 200 Ok response
   *
   * @param type The name of the JSON API data type.
   * @param options Serialization options (https://github.com/SeyZ/jsonapi-serializer)
   * @returns {function()} The output middleware.
   */
  generateOutput(type, defaultOptions) {
    return (req, res, next) => {
      const options = _.cloneDeep(defaultOptions);
      try {
        const data = get(res);
        const status = getStatus(res);
        const shouldSerializeData = this.prepareResponse(status, req, res, options);
        if (!shouldSerializeData) {
          res.end();
          return null;
        }

        this.setResponseHeaders(res);
        if (!data) {
          const message = 'Serialization failed: no data to serialize';
          return next(new IoError(message));
        }

        let extendedOptions = this.setPaginationLinks(req, res, options);
        extendedOptions = this.setMeta(res, options);

        const serializer = new Serializer(type, extendedOptions);
        res.json(serializer.serialize(data));
        return null;
      } catch (err) {
        return next(err);
      }
    };
  }
}

export default new Output();
