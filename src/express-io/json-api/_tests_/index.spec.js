// import { assert } from 'chai';
// import sinon from 'sinon';
//
// import input from '../input';
// import output from '../output';
// import { setupIo, defaultOptions } from '../index';
//
// sinon.assert.expose(assert, { prefix: '' });
//
// describe('JSON API IO', () => {
//   describe('setupIo', () => {
//     const typeName = 'test.types';
//
//     it('throws an error if the type is undefined', () => {
//       assert.throws(() => setupIo(), 'type is a required value');
//     });
//
//     it('returns an object with middleware factory functions', () => {
//       const io = setupIo(typeName);
//       assert.isFunction(io.in());
//       assert.isFunction(io.out());
//     });
//
//     describe('out', () => {
//       it('initializes the output processing with default options', sinon.test(function () {
//         this.stub(output, 'generateOutput');
//
//         const io = setupIo(typeName);
//         io.out();
//
//         assert.calledWith(output.generateOutput, typeName, defaultOptions);
//       }));
//
//       it('initializes the output processing with shared options', sinon.test(function () {
//         this.stub(output, 'generateOutput');
//
//         const sharedOptions = { key: 'value' };
//         const io = setupIo(typeName, sharedOptions);
//         io.out();
//
//         assert.calledWith(output.generateOutput, typeName, {
//           ...defaultOptions,
//           ...sharedOptions,
//         });
//       }));
//
//       it('initializes the output processing with middleware options', sinon.test(function () {
//         this.stub(output, 'generateOutput');
//
//         const sharedOptions = { key: 'value' };
//         const io = setupIo(typeName, sharedOptions);
//
//         const middlewareOptions = { middlewareKey: 'middlewareValue' };
//         io.out(middlewareOptions);
//
//         assert.calledWith(output.generateOutput, typeName, {
//           ...defaultOptions,
//           ...sharedOptions,
//           ...middlewareOptions,
//         });
//       }));
//     });
//
//     describe('in', () => {
//       it('initializes the input processing with default options', sinon.test(function () {
//         this.stub(input, 'processInput');
//
//         const io = setupIo(typeName);
//         io.in();
//
//         assert.calledWith(input.processInput, typeName, defaultOptions);
//       }));
//
//       it('initializes the input processing with shared options', sinon.test(function () {
//         this.stub(input, 'processInput');
//
//         const sharedOptions = { key: 'value' };
//         const io = setupIo(typeName, null, sharedOptions);
//         io.in();
//
//         assert.calledWith(input.processInput, typeName, {
//           ...defaultOptions,
//           ...sharedOptions,
//         });
//       }));
//
//       it('initializes the input processing with middleware options', sinon.test(function () {
//         this.stub(input, 'processInput');
//
//         const sharedOptions = { key: 'value' };
//         const io = setupIo(typeName, null, sharedOptions);
//
//         const middlewareOptions = { middlewareKey: 'middlewareValue' };
//         io.in(middlewareOptions);
//
//         assert.calledWith(input.processInput, typeName, {
//           ...defaultOptions,
//           ...sharedOptions,
//           ...middlewareOptions,
//         });
//       }));
//     });
//   });
// });
