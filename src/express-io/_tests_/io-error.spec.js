import { assert } from 'chai';

import { IoError, ioErrorAdapter } from '../io-error';

describe('IO error', () => {
  describe('error adapter', () => {
    it('ignores other errors', () => {
      const error = new Error('Some other error');
      assert.isNotOk(ioErrorAdapter.toJsonApi(error));
    });

    it('handles IO errors', () => {
      const error = new IoError('Io error');
      assert.isObject(ioErrorAdapter.toJsonApi(error));
    });

    it('includes the information from the IO error', () => {
      const error = new IoError('Io error');
      assert.deepEqual(ioErrorAdapter.toJsonApi(error), {
        status: 400,
        title: 'Bad request',
        detail: error.message,
        meta: {
          trace: error,
        },
      });
    });
  });
});
