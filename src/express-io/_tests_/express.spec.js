import { assert } from 'chai';
import { createRequest } from 'node-mocks-http';

import {
  getFilter, setFilter,
  getSort, setSort,
  getPage, setPage,
  getMeta, setMeta,
} from '../express';

describe('getFilter', () => {
  it('returns correctly when filter is not set', () => {
    const req = createRequest();
    const ret = getFilter(req);
    assert.isUndefined(ret);
  });

  it('returns correct value when filter is set', () => {
    const req = createRequest();
    const filter = { field: ['value1'] };

    setFilter(req, filter);

    assert.deepEqual(getFilter(req), { field: ['value1'] });
  });
});

describe('setFilter', () => {
  it('sets filter correctly', () => {
    const req = createRequest();
    const filter = { field: ['value1', 'value2'] };

    setFilter(req, filter);

    assert.deepEqual(req.locals.io.filter, { field: ['value1', 'value2'] });
  });
});

describe('getSort', () => {
  it('returns correctly when sort is not set', () => {
    const req = createRequest();
    const ret = getSort(req);
    assert.isUndefined(ret);
  });

  it('returns correct value when sort is set', () => {
    const req = createRequest();
    const sort = 'name';

    setSort(req, sort);

    assert.deepEqual(getSort(req), 'name');
  });
});

describe('setSort', () => {
  it('sets sort correctly', () => {
    const req = createRequest();
    const sort = 'name';

    setSort(req, sort);

    assert.deepEqual(req.locals.io.sort, 'name');
  });
});

describe('getPage', () => {
  it('returns correctly when page is not set', () => {
    const req = createRequest();
    const ret = getPage(req);
    assert.isUndefined(ret);
  });

  it('returns correct value when page is set', () => {
    const req = createRequest();
    const page = { limit: 1 };

    setPage(req, page);

    assert.deepEqual(getPage(req), { limit: 1 });
  });
});

describe('setPage', () => {
  it('sets page correctly', () => {
    const req = createRequest();
    const page = { limit: 1 };

    setSort(req, page);

    assert.deepEqual(req.locals.io.sort, { limit: 1 });
  });
});

describe('getMeta', () => {
  it('returns correctly when meta is not set', () => {
    const req = createRequest();
    const ret = getMeta(req);
    assert.isUndefined(ret);
  });

  it('returns correct value when meta is set', () => {
    const req = createRequest();
    const meta = { count: 123 };

    setMeta(req, meta);

    assert.deepEqual(getMeta(req), { count: 123 });
  });
});

describe('setMeta', () => {
  it('sets meta correctly', () => {
    const req = createRequest();
    const meta = { count: 123 };

    setMeta(req, meta);

    assert.deepEqual(req.locals.io.meta, { count: 123 });
  });
});
