export function resolveClientTopic(clientId) {
  return `client.${clientId}`;
}

export function resolveGuideTopic(guideId) {
  return `guide.${guideId}`;
}

export function resolveBroadcastTopic() {
  return 'broadcast';
}
