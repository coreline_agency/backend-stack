import firebaseApp from '../../firebase/firebase-app';
import {
  resolveClientTopic,
  resolveGuideTopic,
  resolveBroadcastTopic,
} from './topic';

const OPTIONS = {
  priority: 'high',
};

export function createPayload(body = '', action = '') {
  const payload = {
    notification: {
      body,
      clickAction: action,
      sound: 'default',
      badge: '1',
    },
  };

  return payload;
}

export function sendToClient(clientId, payload) {
  const topic = resolveClientTopic(clientId);
  return firebaseApp.messaging().sendToTopic(topic, payload, OPTIONS);
}

export function sendToGuide(guideId, payload) {
  const topic = resolveGuideTopic(guideId);
  return firebaseApp.messaging().sendToTopic(topic, payload, OPTIONS);
}

export function sendBroadcast(payload) {
  const topic = resolveBroadcastTopic();
  return firebaseApp.messaging().sendToTopic(topic, payload, OPTIONS);
}
