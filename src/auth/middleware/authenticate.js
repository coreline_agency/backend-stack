import { asyncMiddleware, setLocals } from '../../express-stack';
import { knownErrors } from '../../express-jsonapi-error-middleware';
import firebaseApp from '../../firebase/firebase-app';

export default () => asyncMiddleware(async(req) => {
  let token;
  if (req.headers && req.headers.authorization) {
    const parts = req.headers.authorization.split(' ');
    const scheme = parts[0];
    const credentials = parts[1] || '';
    if (/^Bearer$/i.test(scheme)) {
      token = credentials;
    }
  }
  if (!token) {
    return;
  }

  try {
    const decodedToken = await firebaseApp.auth().verifyIdToken(token);
    setLocals(req, 'auth.token', decodedToken);
  } catch (err) {
    throw new knownErrors.NotAuthorizedError();
  }
});
