import { asyncMiddleware, getLocals, setLocals } from '../../express-stack';
import firebaseApp from '../../firebase/firebase-app';

export default () => asyncMiddleware(async(req) => {
  const uid = getLocals(req, 'auth.token.uid');
  if (uid) {
    const user = await firebaseApp.auth().getUser(uid);
    setLocals(req, 'auth.user', user);
  }
});
