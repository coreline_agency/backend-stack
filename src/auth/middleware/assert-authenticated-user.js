import { asyncMiddleware, getLocals } from '../../express-stack';
import { knownErrors } from '../../express-jsonapi-error-middleware';

export default () => asyncMiddleware(async(req) => {
  const user = getLocals(req, 'auth.user');
  if (!user) {
    throw new knownErrors.NotAuthorizedError();
  }
});
