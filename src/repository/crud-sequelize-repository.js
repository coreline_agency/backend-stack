import { lean } from '../db/leanDecorator';

/**
 * Repository providing basic CRUD operations for Sequelize model.
 * Optional features like include can be turned on and customized via options.
 */
export class CrudSequelizeRepository {
  /**
   * Constructor
   * @param Model Sequelize data model
   * @param options Object which can contain:
   *  - 'include' array of objects, include option description:
   *  http://docs.sequelizejs.com/manual/tutorial/models-usage.html#top-level-where-with-eagerly-loaded-models
   */
  constructor(Model, options = {}) {
    this.Model = Model;
    this.options = options;

    this.getAll = this.getAll.bind(this);
    this.get = this.get.bind(this);
    this.create = this.create.bind(this);
    this.update = this.update.bind(this);
    this.remove = this.remove.bind(this);
  }

  @lean
  async getAll() {
    return this.Model.findAll({ include: this.options.include });
  }

  @lean
  async get(id) {
    return this.Model.findOne({ where: { id }, include: this.options.include });
  }

  @lean
  async create(data) {
    const createdModel = await this.Model.create(data);
    return this.get(createdModel.id);
  }

  @lean
  async update(id, data) {
    await this.Model.update(data, { where: { id } });
    return this.get(id);
  }

  remove(id) {
    return this.Model.destroy({ where: { id } });
  }
}
