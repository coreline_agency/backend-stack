import * as _ from 'lodash';

import { unknownErrorAdapter } from './adapters/unknown-error-adapter';
import { knownErrorAdapter } from './adapters/known-error-adapter';
import { serverErrorAdapter } from './adapters/server-error-adapter';
import { apiErrorAdapter } from './adapters/api-error-adapter';
import * as allKnownErrors from './known-errors';

/*
 exposing known errors for usage.
 */
export const knownErrors = allKnownErrors;

/*
 A 404 response handler if routes are missed but only if the response did not set any headers.
 */
export function defaultNotFound(req, res, next) {
  // send 404 only if response has not been prepared by other middleware
  if (res.headersSent) {
    next();
  } else {
    next(new allKnownErrors.NotFoundError());
  }
}

export function errorHandler(settings = { showFullError: false }) {
  const adapters = settings.customAdapters || [];

  adapters.push(apiErrorAdapter);
  adapters.push(knownErrorAdapter);
  adapters.push(serverErrorAdapter);
  adapters.push(unknownErrorAdapter);
  return (err, req, res, next) => {
    try {
      let i = 0;
      let error = null;
      do {
        error = adapters[i++].toJsonApi(err);
      } while (!error);

      // According to JSON API spec, error response always contains an array
      // of errors.
      const errors = _.isArray(error) ? error : [error];
      const status = parseInt(errors[0].status, 10);
      if (!settings.showFullError) {
        for (const e of errors) {
          delete e.detail;
          delete e.meta;
        }
      }
      res.status(status).json({ errors });
      next({ errors });
    } catch (exc) {
      const errors = {
        errors: [{
          title: 'server error',
          status: '500',
        }],
      };
      if (settings.showFullError) {
        errors.errors[0].meta = {
          trace: exc,
        };
      }
      res.status(500).json(errors);
      next(errors);
    }
  };
}
