/* eslint-disable no-unused-expressions */
import chai from 'chai';
import * as _ from 'lodash';

import {
  ApiError,
  IoError,
  InvalidIdError,
  NodeError,
  HttpError,
  NotAuthorizedError,
  ForbiddenError,
  NotFoundError,
  ValidationError,
} from '../known-errors';

const expect = chai.expect;

describe('known-errors', () => {
  describe('NodeError', () => {
    it('should inherit from error, capture stack and take arguments', () => {
      expect(NodeError).to.be.ok;
      const detail = _.random(1, 10) % 2 === 0 ? 'testDetail' : '';
      const title = _.random(1, 10) % 2 === 0 ? 'some title' : undefined;
      const nodeErr = new NodeError(detail, title);
      expect(nodeErr).to.be.ok.and.instanceOf(Error);
      expect(nodeErr.detail).to.be.a('string').and.to.eql(detail);
      expect(nodeErr.title).to.be.a('string').and.to.eql(title || 'node error');
      expect(nodeErr.stack).to.be.a('string').and.to.be.ok;
    });
  });

  describe('HttpError', () => {
    it('should inherit from error, capture stack and take arguments', () => {
      expect(HttpError).to.be.ok;
      const detail = _.random(1, 10) % 2 === 0 ? 'testDetail' : '';
      const title = _.random(1, 10) % 2 === 0 ? 'some title' : undefined;
      const status = _.random(200, 510);
      const httpErr = new HttpError(status, detail, title);
      expect(httpErr).to.be.ok.and.instanceOf(Error).and.to.be.instanceOf(NodeError);
      expect(httpErr.detail).to.be.a('string').and.to.eql(detail);
      expect(httpErr.title).to.be.a('string').and.to.eql(title || 'http error');
      expect(httpErr.stack).to.be.a('string').and.to.be.ok;
      expect(httpErr.status).to.be.a('string').and.to.eql(status.toString());
    });

    it('should default to 500 status code', () => {
      const httpErr = new HttpError();
      expect(httpErr).to.be.ok.and.instanceOf(Error).and.to.be.instanceOf(NodeError);
      expect(httpErr.detail).to.be.a('string').and.to.eql('');
      expect(httpErr.title).to.be.a('string').and.to.eql('http error');
      expect(httpErr.stack).to.be.a('string').and.to.be.ok;
      expect(httpErr.status).to.be.a('string').and.to.eql('500');
    });
  });

  describe('NotAuthorizedError', () => {
    it('should inherit from http error and have 401 status', () => {
      const naErr = new NotAuthorizedError();
      expect(naErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(naErr.detail).to.be.a('string').and.to.eql('');
      expect(naErr.title).to.be.a('string').and.to.eql('not authorized');
      expect(naErr.stack).to.be.a('string').and.to.be.ok;
      expect(naErr.status).to.be.a('string').and.to.eql('401');
    });
  });

  describe('ForbiddenError', () => {
    it('should inherit from http error and have 403 status', () => {
      const fbdErr = new ForbiddenError();
      expect(fbdErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(fbdErr.detail).to.be.a('string').and.to.eql('');
      expect(fbdErr.title).to.be.a('string').and.to.eql('forbidden');
      expect(fbdErr.stack).to.be.a('string').and.to.be.ok;
      expect(fbdErr.status).to.be.a('string').and.to.eql('403');
    });
  });

  describe('NotFoundError', () => {
    it('should inherit from http error and have 404 status', () => {
      const nfErr = new NotFoundError();
      expect(nfErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(nfErr.detail).to.be.a('string').and.to.eql('');
      expect(nfErr.title).to.be.a('string').and.to.eql('not found');
      expect(nfErr.stack).to.be.a('string').and.to.be.ok;
      expect(nfErr.status).to.be.a('string').and.to.eql('404');
    });
  });

  describe('ValidationError', () => {
    it('should inherit from http error and have 400 status', () => {
      const valErr = new ValidationError();
      expect(valErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(valErr.detail).to.be.a('string').and.to.eql('');
      expect(valErr.title).to.be.a('string').and.to.eql('validation error');
      expect(valErr.stack).to.be.a('string').and.to.be.ok;
      expect(valErr.status).to.be.a('string').and.to.eql('400');
    });
  });

  describe('InvalidIdError', () => {
    it('should inherit from http error and have 400 status', () => {
      const invIdErr = new InvalidIdError();
      expect(invIdErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(invIdErr.detail).to.be.a('string').and.to.eql('');
      expect(invIdErr.title).to.be.a('string').and.to.eql('invalid id');
      expect(invIdErr.stack).to.be.a('string').and.to.be.ok;
      expect(invIdErr.status).to.be.a('string').and.to.eql('400');
    });
  });

  describe('IoError', () => {
    it('should inherit from http error and have default 500 status', () => {
      const ioErr = new IoError();
      expect(ioErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(ioErr.detail).to.be.a('string').and.to.eql('');
      expect(ioErr.title).to.be.a('string').and.to.eql('io error');
      expect(ioErr.stack).to.be.a('string').and.to.be.ok;
      expect(ioErr.status).to.be.a('string').and.to.eql('500');
    });

    it('should take in status code as argument', () => {
      const testCode = '400';
      const ioErr = new IoError('test', testCode);
      expect(ioErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(ioErr.detail).to.be.a('string').and.to.eql('test');
      expect(ioErr.title).to.be.a('string').and.to.eql('io error');
      expect(ioErr.stack).to.be.a('string').and.to.be.ok;
      expect(ioErr.status).to.be.a('string').and.to.eql(testCode);
    });
  });

  describe('ApiError', () => {
    it('should inherit from http error and have default 500 status', () => {
      const apiErr = new ApiError();
      expect(apiErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(apiErr.detail).to.be.a('string')
        .and.to.eql('Unexpected response while performing API call.');

      expect(apiErr.title).to.be.a('string').and.to.eql('api error');
      expect(apiErr.stack).to.be.a('string').and.to.be.ok;
      expect(apiErr.status).to.be.a('string').and.to.eql('500');
    });

    it('should take in response object as argument', () => {
      const response = { req: { body: 'test' } };
      const apiErr = new ApiError(response);
      expect(apiErr).to.be.ok.and.instanceOf(Error)
        .and.to.be.instanceOf(NodeError)
        .and.to.be.instanceOf(HttpError);

      expect(apiErr.detail).to.be.a('string');
      expect(apiErr.title).to.be.a('string').and.to.eql('api error');
      expect(apiErr.stack).to.be.a('string').and.to.be.ok;
      expect(apiErr.status).to.be.a('string').and.to.eql('500');
      expect(apiErr.response).to.be.an('Object').and.to.eql(response);
    });
  });
});
