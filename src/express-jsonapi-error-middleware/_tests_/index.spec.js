import chai, { assert } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import { defaultNotFound, knownErrors, errorHandler } from '../index';
import * as allKnownErrors from '../known-errors';
chai.use(sinonChai);
const expect = chai.expect;

function prepareResponse() {
  return {
    json: sinon.spy(),
    status: sinon.stub().returnsThis(),
  };
}

describe('errorHandler', () => {
  it('exposes known errors', () => {
    expect(knownErrors).to.be.an('object').and.to.be.ok.and.to.deep.equal(allKnownErrors);
    for (const key of Object.keys(knownErrors)) {
      const err = new knownErrors[key];

      expect(err).to.have.property('title').that.is.a('string');
      expect(err).to.have.property('detail').that.is.a('string');
      expect(err).to.have.property('stack').that.is.a('string');

      if (err instanceof knownErrors.HttpError) {
        expect(err).to.have.property('status').that.is.a('string');
      }
    }
  });

  it('exposes default not found handler', (done) => {
    expect(defaultNotFound).to.be.a('function');

    const res = {
      headersSent: false,
    };
    defaultNotFound(null, res, (err) => {
      try {
        assert.instanceOf(err, allKnownErrors.NotFoundError);
        done();
      } catch (err) {
        done(err);
      }
    });
  });

  it('exposes default not found handler which calls `next` without error if headers sent', () => {
    expect(defaultNotFound).to.be.a('function');

    const res = {
      headersSent: true,
    };
    const next = sinon.spy();
    defaultNotFound(null, res, next);
    assert.calledOnce(next);
  });

  it('returns 500 in case of any exceptions', () => {
    const res = prepareResponse();
    const errorThrowingAdapter = {
      toJsonApi: sinon.stub().throws(new Error('test')),
    };
    const handler = errorHandler({ customAdapters: [errorThrowingAdapter] });
    const next = sinon.spy();

    handler(null, null, res, next);

    const errorsOut = {
      errors: [{
        title: 'server error',
        status: '500',
      }],
    };
    expect(res.status).to.have.been.calledWith(500);
    expect(res.json).to.have.been.calledWithExactly(errorsOut);
    expect(next).to.have.been.calledWithExactly(errorsOut);
  });

  // it('returns an error in case of any errors that are known', () => {
  //   Object.keys(knownErrors).forEach((errorKey) => {
  //     const res = prepareResponse();
  //     const err = new knownErrors[errorKey]('test');
  //     const handler = errorHandler({ showFullError: true });
  //     const next = sinon.spy();
  //     handler(err, null, res, next);
  //
  //     expect(res.status).to.have.been.calledWith(parseInt(err.status, 10));
  //     const expectedErrors = {
  //       errors: [
  //         {
  //           detail: err.detail,
  //           title: err.title,
  //           status: err.status,
  //           meta: {
  //             trace: err,
  //           },
  //         },
  //       ],
  //     };
  //     if (err instanceof knownErrors.ApiError) {
  //       expectedErrors.errors[0].meta.trace = _.omit(err, ['status', 'detail', 'title']);
  //     }
  //     expect(res.json).to.have.been.calledWithExactly(expectedErrors);
  //     expect(next).to.have.been.calledWithExactly(expectedErrors);
  //   });
  // });

  it('it uses default adapters, and responds', () => {
    const req = null;
    const next = sinon.spy();
    const res = prepareResponse();
    const handler = errorHandler({
      showFullError: true,
    });
    const err = new Error('hello, world of errors');
    const expectedError = {
      title: 'server error',
      status: '500',
      meta: {
        trace: {
          detail: err.message,
          title: err.name,
          stack: err.stack,
        },
      },
    };

    handler(err, req, res, next);

    expect(res.status).to.have.been.calledWith(500);
    expect(res.json).to.have.been.calledWithExactly({ errors: [expectedError] });
    expect(next).to.have.been.calledWithExactly({ errors: [expectedError] });
  });

  it('it uses the given settings to handle the error', () => {
    class MyError {
      constructor(message) {
        this.message = message;
      }
    }
    const expectedError = {
      status: '987',
      title: 'my error',
    };
    const req = null;
    const next = sinon.spy();
    const res = prepareResponse();
    const handler = errorHandler({
      customAdapters: [{
        toJsonApi: err => (err instanceof MyError
          ? {
            status: '987',
            title: 'my error',
            detail: 'some error detail which will be removed',
          }
          : null),
      }],
    });
    const err = new MyError('msg');

    handler(err, req, res, next);

    expect(res.status).to.have.been.calledWith(987);
    expect(res.json).to.have.been.calledWithExactly({ errors: [expectedError] });
    expect(next).to.have.been.calledWithExactly({ errors: [expectedError] });
  });
});
