import { expect } from 'chai';
import { knownErrorAdapter } from '../known-error-adapter';
import { knownErrors } from '../../index';

describe('known-error-adapter', () => {
  describe('knownErrorAdapter', () => {
    it('passes over errors of String type', () => {
      const errorIn = new Error('whatever');
      const error = knownErrorAdapter.toJsonApi(errorIn);
      expect(error).to.be.null; // eslint-disable-line no-unused-expressions
    });

    it('handles instances of known errors', () => {
      for (const errType of Object.keys(knownErrors)) {
        const errorIn = new knownErrors[errType];
        const errorOut = {
          status: errorIn.status,
          title: errorIn.title,
          detail: errorIn.detail,
          meta: {
            trace: errorIn,
          },
        };

        const error = knownErrorAdapter.toJsonApi(errorIn);
        delete error.code;
        expect(error).to.deep.equal(errorOut);
      }
    });
  });
});
