import { expect } from 'chai';
import { apiErrorAdapter } from '../api-error-adapter';
import { ApiError } from '../../known-errors';

describe('api-error-adapter', () => {
  describe('apiErrorAdapter', () => {
    it('passes down instances of Error', () => {
      // eslint-disable-next-line no-unused-expressions
      expect(apiErrorAdapter.toJsonApi(new Error())).to.be.null;
    });

    it('passes null for instances of String', () => {
      // eslint-disable-next-line no-unused-expressions
      expect(apiErrorAdapter.toJsonApi('error!')).to.be.null;
    });

    it('handles ApiError', () => {
      const response = { statusCode: 400, body: {}, headers: { accept: 'application/json' } };
      const errorIn = new ApiError(response);
      const errorOut = {
        code: 'api_default',
        status: response.statusCode.toString(),
        title: 'api error',
        detail: errorIn.detail,
        meta: {
          trace: { code: 'api_default', response },
        },
      };

      const error = apiErrorAdapter.toJsonApi(errorIn);
      expect(error).to.deep.equal(errorOut);
    });

    it('defaults status to 500 if not found in response', () => {
      const response = { };
      const errorIn = new ApiError(response);
      const errorOut = {
        code: 'api_default',
        status: '500',
        title: 'api error',
        detail: errorIn.detail,
        meta: {
          trace: { code: 'api_default', response },
        },
      };

      const error = apiErrorAdapter.toJsonApi(errorIn);
      expect(error).to.deep.equal(errorOut);
    });

    it('gets status code from response\'s body', () => {
      const response = { body: { http_status_code: 421 } };
      const errorIn = new ApiError(response);
      const errorOut = {
        code: 'api_default',
        status: '421',
        title: 'api error',
        detail: errorIn.detail,
        meta: {
          trace: { code: 'api_default', response },
        },
      };

      const error = apiErrorAdapter.toJsonApi(errorIn);
      expect(error).to.deep.equal(errorOut);
    });

    it('works without a supplied response', () => {
      const errorIn = new ApiError();
      const errorOut = {
        code: 'api_default',
        status: '500',
        title: 'api error',
        detail: errorIn.detail,
        meta: {
          trace: null,
        },
      };

      const error = apiErrorAdapter.toJsonApi(errorIn);
      expect(error).to.deep.equal(errorOut);
    });
  });
});
