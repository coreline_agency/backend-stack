import { expect } from 'chai';
import { unknownErrorAdapter } from '../unknown-error-adapter';

describe('unknown-error-adapter', () => {
  describe('unknownErrorAdapter', () => {
    it('handles errors of String type', () => {
      const errorIn = 'whatever';
      const errorOut = {
        status: '500',
        title: 'server error',
        meta: {
          trace: JSON.stringify(errorIn),
        },
      };

      const error = unknownErrorAdapter.toJsonApi(errorIn);
      expect(error).to.deep.equal(errorOut);
    });

    it('handles instances of Error', () => {
      const errorIn = new Error('whatever');
      const errorOut = {
        status: '500',
        title: 'server error',
        meta: {
          trace: {
            detail: errorIn.message,
            title: errorIn.name,
            stack: errorIn.stack,
          },
        },
      };

      const error = unknownErrorAdapter.toJsonApi(errorIn);
      expect(error).to.deep.equal(errorOut);
    });
  });
});
