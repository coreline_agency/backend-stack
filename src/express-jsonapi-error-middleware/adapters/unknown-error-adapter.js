function getTrace(err) {
  let error = err;
  if (err instanceof Error) {
    error = {
      title: err.name,
      detail: err.message,
      stack: err.stack,
    };
  } else {
    try {
      error = JSON.stringify(err);
    } catch (e) {
      error = err;
    }
  }
  return error;
}

export const unknownErrorAdapter = {
  toJsonApi: (err) => ({
    status: '500',
    title: 'server error',
    meta: {
      trace: getTrace(err),
    },
  }),
};
