import * as knownErrors from '../known-errors';
const ERROR_KEYS = Object.keys(knownErrors);
export const knownErrorAdapter = {
  toJsonApi: (err) => {
    let error = null;
    for (const errorKey of ERROR_KEYS) {
      if (err instanceof knownErrors[errorKey]) {
        error = {
          status: err.status,
          title: err.title,
          detail: err.detail,
          code: err.code,
          meta: {
            trace: err,
          },
        };
        break;
      }
    }
    return error;
  },
};
