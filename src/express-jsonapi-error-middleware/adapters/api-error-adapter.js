import * as _ from 'lodash';
import { ApiError } from '../known-errors';

function getStatusCode(response) {
  let status = _.get(response, 'statusCode');
  if (!status) {
    status = _.get(response, 'body.http_status_code');
  }
  return status || 500;
}
export const apiErrorAdapter = {
  toJsonApi: (err) => {
    if (err instanceof ApiError) {
      return {
        status: getStatusCode(err.response).toString(),
        title: 'api error',
        detail: err.detail,
        code: err.code,
        meta: {
          trace: err.response ? _.omit(err, ['status', 'detail', 'title']) : null,
        },
      };
    }
    // must return null so other adapters can try and recognize err
    return null;
  },
};
