import { VError, WError } from 'verror';

export const serverErrorAdapter = {
  toJsonApi: (err) => {
    if ((err instanceof VError) || (err instanceof WError)) {
      return {
        status: '500',
        title: 'server error',
        detail: err.message,
        meta: {
          trace: err.toString(),
        },
      };
    }
    return null;
  },
};
